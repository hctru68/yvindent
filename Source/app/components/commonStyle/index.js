import {
  StyleSheet
} from 'react-native';
import Size from '../../common/constants/size';
import { PROGRESS_INDICATOR_BG } from '../../common/constants/color';

const commontStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: Size.width,
    height: Size.height,
    backgroundColor: PROGRESS_INDICATOR_BG,
  },
});

export default commontStyles;
