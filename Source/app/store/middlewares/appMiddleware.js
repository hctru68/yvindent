import NavigatorService from '../../services/navigator/AppNavigatorService';

function* navigateToSendMailPage() {
  console.log('Move To SomeThing Page');

  yield NavigatorService.navigate('SendMailPage');
}

const appMiddleware = {
    navigateToSendMailPage,
};

export default appMiddleware;
