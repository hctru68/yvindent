import {
  put,
  call,
} from 'redux-saga/effects';

import restApi from '../../services/api';

import NavigatorService from '../../services/navigator/AppNavigatorService';
import HandlerException from '../../common/utils/exceptionHelper';
import I18n from '../../common/utils/languageHelper';
import LocalStorage from '../../services/storage';
import { USER_INFO, TOKEN_USER } from '../../common/constants/store';
import { APP_ACTIONS, USER_ACTIONS } from '../actionTypes';
import { API_LOGIN, API_UPDATE_USER } from '../../common/constants/url';

import userStateObject from '../../common/objects/userStateObject';
import { errorObject } from '../../services/responseHandlers/errorHandler';

function* handlerCheckUserExisted() {
  const user = yield LocalStorage.get(TOKEN_USER);
  if (!user) {
    yield put({
      type: APP_ACTIONS.LOGIN_SCREEN,
    });
    yield NavigatorService.reset('Login');
    return yield null;
  }
  yield NavigatorService.reset('Home');
  yield put({
    type: USER_ACTIONS.REQUEST_LOGIN_SUCCESS,
    data: user,
  });
  return yield null;
}

function* handlerCallLogin(action) {
  try {
    const data = {
      username: action.username,
      password: action.password,
    };
    const result = yield restApi.post(API_LOGIN, data);
    console.log('RESULT: ', result);
    if (result.status === 'success') {
      const tokenData = userStateObject.wrapUserLoginData(result);
      yield LocalStorage.save(TOKEN_USER, tokenData);
      yield put({
        type: USER_ACTIONS.REQUEST_LOGIN_SUCCESS,
        data: tokenData,
      });
    } else {
      const error = errorObject(401, I18n.t('login_error.login_fail'));
      yield put(HandlerException.handlerException(
        error,
        USER_ACTIONS.REQUEST_LOGIN_FAILURE,
        error.message,
      ));
    }
  } catch (error) {
    const { code, message } = error;
    console.log('Error: ', error);

    let messageError = '';
    switch (code) {
      case 400:
        messageError = I18n.t('');
        break;
      default:
        messageError = message;
    }
    yield put(HandlerException.handlerException(
      error,
      USER_ACTIONS.REQUEST_LOGIN_FAILURE,
      messageError,
    ));
  }
}

function* moveToHome() {
  yield put({ type: APP_ACTIONS.HOME_SCREEN });
  yield NavigatorService.reset('Home');
}

function* hnadlerCallLogout() {
  yield put({ type: APP_ACTIONS.LOGIN_SCREEN });
  yield LocalStorage.delete(TOKEN_USER);
  yield NavigatorService.reset('Login');
}

function* handlerCallUpdateUser(action) {
  try {
    const data = {
      user_info: action.userinfo,
    };
    // const result = yield restApi.post(API_UPDATE_USER, data);
    // TODO: If success put({ type: USER_ACTIONS.REQUEST_USER_UPDATE_SUCCESS , userdata })

  } catch (error) {
    const { code, message } = error;
    console.log('Error: ', error);

    let messageError = '';
    switch (code) {
      case 400:
        messageError = I18n.t('');
        break;
      default:
        messageError = message;
    }
    yield put(HandlerException.handlerException(
      error,
      USER_ACTIONS.REQUEST_LOGIN_FAILURE,
      messageError,
    ));
  }
}

const userMiddlerware = {
  handlerCheckUserExisted,
  handlerCallLogin,
  moveToHome,
  hnadlerCallLogout,
  handlerCallUpdateUser,
};

export default userMiddlerware;
