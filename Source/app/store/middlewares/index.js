import {
    all,
    call,
    take,
    takeLatest,
    fork,
} from 'redux-saga/effects';
import {
    USER_ACTIONS,
    INBOX_ACTIONS,
    PROFILE_ACTIONS,
    APP_ACTIONS,
    CALENDAR_ACTIONS,
    SEND_MAIL_ACTIONS,
    GET_LIST_SEND_ACTIONS,
    DETAILMAIL_ACTIONS,
} from '../actionTypes';
import userMiddlerware from './userMiddlerware';
import inboxMiddlerware from './inboxMiddlerware';
import detailMailMiddlerware from './detailMailMiddlerware';
import profileMiddlerware from './profileMiddlerware';
import appMiddleware from './appMiddleware';
import sendMailMiddleware from './sendMailMiddleware';
import calendarMiddleware from './calendarMiddleware';

function* actionCheckUserExist() {
    yield takeLatest(USER_ACTIONS.REQUEST_CHECK_EXISTED_USER,
        userMiddlerware.handlerCheckUserExisted);
}

function* actionLogin() {
    yield takeLatest(USER_ACTIONS.REQUEST_LOGIN, userMiddlerware.handlerCallLogin);
}

function* actionLoginSuccess() {
    yield takeLatest(USER_ACTIONS.REQUEST_LOGIN_SUCCESS, userMiddlerware.moveToHome);
}

function* actionLogout() {
    yield takeLatest(USER_ACTIONS.REQUEST_LOGOUT, userMiddlerware.hnadlerCallLogout);
}

function* actionUpdateUser() {
    yield takeLatest(USER_ACTIONS.REQUEST_USER_UPDATE, userMiddlerware.handlerCallUpdateUser);
}

//Profile
function* actionGetAllProfile() {
    yield takeLatest(PROFILE_ACTIONS.REQUEST_PROFILE_GETALL, profileMiddlerware.handlerGetAllProfile);
}
function* actionSendMail() {
    yield takeLatest(SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL, sendMailMiddleware.handlerSendMail);
}

function* actionUpdateProfile() {
    yield takeLatest(PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE, profileMiddlerware.handlerUpdateProfile);
}

function* actionUploadBase64() {
    yield takeLatest(PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64, profileMiddlerware.handlerUploadBase64);

}
function* actionUploadBase64Inbox() {
    yield takeLatest(SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64, sendMailMiddleware.handlerUploadBase64Inbox);

}
//Inbox
function* actionGetAllInbox() {
    yield takeLatest(INBOX_ACTIONS.REQUEST_INBOX_GETALL, inboxMiddlerware.handlerGetAllInbox);
}
function* actionDeleteByIdInbox() {
    yield takeLatest(INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID, inboxMiddlerware.handlerDeleteByIdInbox);
}

function* actionOpenSendMailPage() {
    yield takeLatest(APP_ACTIONS.SEND_MAIL, appMiddleware.navigateToSendMailPage);
}
//Detail mail
function* actionOpenDetailMailPage() {
    yield takeLatest(DETAILMAIL_ACTIONS.PUT_ISREAD_DETAILMAIL, detailMailMiddlerware.handlerPutIsReadMail);
}


//Calender
function* actionGetCalendarInfo() {
    yield takeLatest(CALENDAR_ACTIONS.REQUEST_CALENDAR, calendarMiddleware.getCalendarInfo);
}
function* actionGetListSend() {
    yield takeLatest(GET_LIST_SEND_ACTIONS.REQUEST_GET_LIST_SEND, sendMailMiddleware.handlerGetListSend);
}
export default function* rootSaga() {
    yield all([
        actionCheckUserExist(),
        actionLogin(),
        actionLoginSuccess(),
        actionUpdateUser(),
        actionLogout(),
        actionGetAllInbox(),

        //profile
        actionGetAllProfile(),
        actionUpdateProfile(),
        actionUploadBase64(),
        //Email
        actionOpenSendMailPage(),
        actionSendMail(),
        actionGetListSend(),
        actionOpenDetailMailPage(),
        actionDeleteByIdInbox(),
        //Calendar
        actionGetCalendarInfo(),
        actionUploadBase64Inbox(),
    ]);
}
