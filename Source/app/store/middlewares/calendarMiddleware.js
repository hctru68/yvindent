import {
  put,
} from 'redux-saga/effects';

import restApi from '../../services/api';

import HandlerException from '../../common/utils/exceptionHelper';
import LocalStorage from '../../services/storage';
import I18n from '../../common/utils/languageHelper';

import { CALENDAR_ACTIONS } from '../actionTypes';
import { API_GET_CALENDAR } from '../../common/constants/url';
import { TOKEN_USER } from '../../common/constants/store';
import calendarStateObject from '../../common/objects/calendarStateObject';


function* getCalendarInfo() {
  try {
    // TODO: CALL API GET CALENDAR INFO
    const token = yield LocalStorage.get(TOKEN_USER);
    const data = {
      token: token.token,
    };

    const result = yield restApi.post(API_GET_CALENDAR, data);
    console.log('RESULT: ', result);
    if (result.status === 'success') {
      const calendarData = yield calendarStateObject.setCalendarData(result.data);
      yield put({ type: CALENDAR_ACTIONS.REQUEST_CALENDAR_SUCCESS, calendarData });
    }
  } catch (error) {
    const { code, message } = error;
    console.log('Error: ', error);

    let messageError = '';
    switch (code) {
      case 400:
        messageError = I18n.t('');
        break;
      default:
        messageError = message;
    }
    yield put(HandlerException.handlerException(
      error,
      CALENDAR_ACTIONS.REQUEST_CALENDAR_FAILURE,
      messageError,
    ));
  }
}

const appMiddleware = {
  getCalendarInfo,
};

export default appMiddleware;
