import {
    put,
} from 'redux-saga/effects';

import restApi from '../../services/api';
import HandlerException from '../../common/utils/exceptionHelper';
import I18n from '../../common/utils/languageHelper';
import LocalStorage from '../../services/storage';
import { TOKEN_USER } from '../../common/constants/store';
import {GET_LIST_SEND_ACTIONS, SEND_MAIL_ACTIONS} from '../actionTypes';
import {API_GET_LIST_SEND, API_SEND_MAIL, API_UPLOAD_FILE} from '../../common/constants/url';
import profileStateObject from "../../common/objects/profileStateObject";
function findAndReplace(string, target, replacement) {
    var i = 0, length = string.length;
    for (i; i < length; i++) {
        string = string.replace(target, replacement);
    }
    return string;
}
function* handlerSendMail(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        let photos = JSON.stringify(action.request_data.photoList);
        let photoList = findAndReplace(photos.substring(2, photos.length - 2),'"','');
        const data = {
            token: token.token,
            listReceive: action.request_data.listReceive,
            Message: action.request_data.Message,
            Subject: action.request_data.Subject,
            photoList: photoList,
        }
        const result = yield restApi.post(API_SEND_MAIL, data);
        if (result.status === 'success') {
            yield put({
                type: SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL_SUCCESS,
            });
        } else {
            yield  put({
                type: SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL_FAILURE,
            })
        }
    }
    catch (error) {
        const { code, message } = error;
        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL_FAILURE,
            messageError,
        ));
    }
}
function* handlerGetListSend(action) {
    try {
        console.log(action);
        var token = yield LocalStorage.get(TOKEN_USER);
        const data = {
            token: token.token
        }
        console.log("API_SEND_MAIL",API_SEND_MAIL);
        const result = yield restApi.post(API_GET_LIST_SEND, data);
        console.log("result",result);
        yield put({
            type: GET_LIST_SEND_ACTIONS.REQUEST_GET_LIST_SEND_SUCCESS,
            result,
        });
    }
    catch (error) {
        const { code, message } = error;
        console.log('Error: ', error);

        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            GET_LIST_SEND_ACTIONS.REQUEST_SEND_MAIL_FAILURE,
            messageError,
        ));
    }
}
function* handlerUploadBase64Inbox(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        const data = {
            token: token.token,
            base64code: action.base64code,
            filename: action.filename
        }
        const result = yield restApi.post(API_UPLOAD_FILE, data);
        if (result.status === 'success') {
            let uploadedData;
            uploadedData = profileStateObject.wrapUploadBase64(result);
            yield put({
                type: SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_SUCCESS,
                uploadedData,
                result
            });
        } else {
            yield put({
                type: SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_FAILURE,
                result,
            });
        }

    } catch (error) {
        const { code, message } = error;
        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_FAILURE,
            messageError,
        ));
    }
}
const sendMailMiddleware = {
    handlerSendMail,
    handlerGetListSend,
    handlerUploadBase64Inbox,
};

export default sendMailMiddleware;
