import {
    put,
    call,
} from 'redux-saga/effects';

import restApi from '../../services/api';

import NavigatorService from '../../services/navigator/AppNavigatorService';
import HandlerException from '../../common/utils/exceptionHelper';
import I18n from '../../common/utils/languageHelper';
import LocalStorage from '../../services/storage';
import { USER_INFO, TOKEN_USER } from '../../common/constants/store';
import { APP_ACTIONS, PROFILE_ACTIONS } from '../actionTypes';
import { API_GETALL_PROFILE, API_UPDATE_PROFILE, API_UPLOAD_BASE64_PROFILE } from '../../common/constants/url';

import profileStateObject from '../../common/objects/profileStateObject';
import { errorObject } from '../../services/responseHandlers/errorHandler';

function* handlerGetAllProfile() {
    try {        
        var token = yield LocalStorage.get(TOKEN_USER);
        //console.log(token);
        const data = {
            token: token.token
        }
        const result = yield restApi.post(API_GETALL_PROFILE, data);
        if (result.status === 'success') {
            let profileData;
            profileData = profileStateObject.wrapProfileData(result);
            yield put({
                type: PROFILE_ACTIONS.REQUEST_PROFILE_GETALL_SUCCESS,
                profileData,
            });
        } 

    } catch (error) {
        const { code, message } = error;
        console.log('Error Profile: ', error);

        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            PROFILE_ACTIONS.REQUEST_PROFILE_GETALL_FAILURE,
            messageError,
        ));
    }
}

function* handlerUpdateProfile(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        const data = {
            token: token.token,
            password: action.password,
            confirmpassword: action.confirmpassword,
            avatar: action.filename,
        }

        

        const result = yield restApi.post(API_UPDATE_PROFILE, data);
        let updateData;
        updateData = profileStateObject.wrapUpdateProfile(result);
        console.log("Middlerware -> handlerUpdateProfile: ");
        console.log("Parameter: ");
        console.log(data);
        console.log("result: ");
        console.log(result);
        if (result.status === 'succes') {
            yield put({
                type: PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_SUCCESS,
                updateData,
                result
            });
        } else {
            yield put({
                type: PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_FAILURE,
                updateData,
                result,
            });
        }

    } catch (error) {
        const { code, message } = error;
        console.log('Error UpdateProfile: ', error);

        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_FAILURE,
            messageError,
        ));
    }
}

function* handlerUploadBase64(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        const data = {
            token: token.token,
            base64code: action.base64code,
            filename: action.filename
        }
        const result = yield restApi.post(API_UPLOAD_BASE64_PROFILE, data);

        console.log("Middlerware -> handlerUploadBase64: ");
        console.log(result);
        if (result.status === 'success') {
            let uploadedData;
            uploadedData = profileStateObject.wrapUploadBase64(result);
            yield put({
                type: PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_SUCCESS,
                uploadedData,
                result
            });
        } else {
            yield put({
                type: PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_FAILURE,
                result,
            });
        }

    } catch (error) {
        const { code, message } = error;
        console.log('Error UploadBase64: ', error);

        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_FAILURE,
            messageError,
        ));
    }
}

const profileMiddlerware = {
    handlerGetAllProfile,
    handlerUpdateProfile,
    handlerUploadBase64,
};

export default profileMiddlerware;
