import {
    put,
    call,
} from 'redux-saga/effects';

import restApi from '../../services/api';

import NavigatorService from '../../services/navigator/AppNavigatorService';
import HandlerException from '../../common/utils/exceptionHelper';
import I18n from '../../common/utils/languageHelper';
import LocalStorage from '../../services/storage';
import { USER_INFO, TOKEN_USER } from '../../common/constants/store';
import { APP_ACTIONS, DETAILMAIL_ACTIONS } from '../actionTypes';
import { API_GETALL_INBOX, API_PUT_ISREAD_MAIL } from '../../common/constants/url';

import detailMailStateObject from '../../common/objects/detailMailStateObject';
import { errorObject } from '../../services/responseHandlers/errorHandler';

function* handlerGetDetailMail(action) {
    try {
        //console.log('handlerGetDetailMail');
        var token = yield LocalStorage.get(TOKEN_USER);
        let inboxId = action.inboxId;
        const data = {
            token: token.token,
            inboxId: inboxId
        }
    } catch (error) {
        console.log('Error: ', error);
    }
}

function* handlerPutIsReadMail(action) {
    try {        
        var token = yield LocalStorage.get(TOKEN_USER);
        let messageId = action.messageId;
        const data = {
            token: token.token,
            MessageId: messageId
        }
        //console.log('handlerPutIsReadMail', data);
        const result = yield restApi.post(API_PUT_ISREAD_MAIL, data);
        //console.log('handlerPutIsReadMail', result);

        if (result.status === 'success') {
            console.log('Update field ISREAD success!');
        } else {
            console.log('Update field ISREAD unsuccess!');
        }
    } catch (error) {
        console.log('Update field ISREAD error 500!');
    }
}
const detailMailMiddlerware = {
    handlerGetDetailMail,
    handlerPutIsReadMail,
};

export default detailMailMiddlerware;
