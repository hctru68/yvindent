import {
    put,
    call,
} from 'redux-saga/effects';

import restApi from '../../services/api';

import NavigatorService from '../../services/navigator/AppNavigatorService';
import HandlerException from '../../common/utils/exceptionHelper';
import I18n from '../../common/utils/languageHelper';
import LocalStorage from '../../services/storage';
import { USER_INFO, TOKEN_USER } from '../../common/constants/store';
import { APP_ACTIONS, INBOX_ACTIONS } from '../actionTypes';
import { API_GETALL_INBOX, API_DELETE_INBOX } from '../../common/constants/url';

import { DEVICE_ID, DEVICE_OS } from '../../common/constants/value';
import inboxStateObject from '../../common/objects/inboxStateObject';
import { errorObject } from '../../services/responseHandlers/errorHandler';

function* handlerGetAllInbox(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        let pNum = action.pageNumber;
        const data = {
            token: token.token,
            curPage: pNum ? pNum : 1
        }
        //console.log('INBOX POST DATA: ', DEVICE_ID, '-----', DEVICE_OS);
        if (pNum) {
            const result = yield restApi.post(API_GETALL_INBOX, data);
            if (result.status === 'success') {
                let inboxData = inboxStateObject.wrapInboxLoginData(result);
                yield put({
                    type: INBOX_ACTIONS.REQUEST_INBOX_GETALL_SUCCESS,
                    inboxData,
                });                
            }
        }
    } catch (error) {
        const { code, message } = error;
        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            INBOX_ACTIONS.REQUEST_INBOX_GETALL_FAILURE,
            messageError,
        ));
    }
}
function* handlerDeleteByIdInbox(action) {
    try {
        var token = yield LocalStorage.get(TOKEN_USER);
        let id = action.id;
        if (id && id > 0) {
            const data = {
                token: token.token,
                MessageId: id
            }
            const result = yield restApi.post(API_DELETE_INBOX, data);
            if (result.status === 'success') {
                //let stateDelete = inboxStateObject.wrapInboxDeleteById(result);
                //yield put({
                //    type: INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID_SUCCESS,
                //    stateDelete,
                //});
                console.log('Delete inbox successful!!!');
            } else {
                const error = errorObject(401, 'Delete fail !!!');
                yield put(HandlerException.handlerException(
                    error,
                    INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID_FAILURE,
                    error.message,
                ));
            }
        } else {
            const error = errorObject(500, 'Error 500: Delete fail !!!');
            yield put(HandlerException.handlerException(
                error,
                INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID_FAILURE,
                error.message,
            ));
        }
    } catch (error) {
        const { code, message } = error;
        let messageError = '';
        switch (code) {
            case 400:
                messageError = I18n.t('');
                break;
            default:
                messageError = message;
        }
        yield put(HandlerException.handlerException(
            error,
            INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID_FAILURE,
            messageError,
        ));
    }
}
const inboxMiddlerware = {
    handlerGetAllInbox,
    handlerDeleteByIdInbox,
};

export default inboxMiddlerware;
