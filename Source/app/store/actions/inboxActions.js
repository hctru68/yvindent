import {
    INBOX_ACTIONS,
    DETAILMAIL_ACTIONS,
} from '../actionTypes';


const getAllInbox = (pageNumber) => ({
    type: INBOX_ACTIONS.REQUEST_INBOX_GETALL,
    pageNumber,
});
const deleteInboxById = (id) => ({
    type: INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID,
    id,
});
const moveToDetailMail = (inboxItem) => ({
    type: DETAILMAIL_ACTIONS.REQUEST_DETAILMAIL_GET,
    inboxItem,
});

const inboxActions = {
    getAllInbox,
    deleteInboxById,
    moveToDetailMail,
};

export default inboxActions;
