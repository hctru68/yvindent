import {
    PROFILE_ACTIONS,
} from '../actionTypes';


const getAllProfile = () => ({
    type: PROFILE_ACTIONS.REQUEST_PROFILE_GETALL
});

const updateProfile = (password, confirmpassword, filename) => ({
    type: PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE,
    password,
    confirmpassword,
    filename,
});

const uploadBase64 = (base64code, filename) => ({
    type: PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64,
    base64code,
    filename,
});

const profileActions = {
    getAllProfile,
    updateProfile,
    uploadBase64
};

export default profileActions;
