import {
    GET_LIST_SEND_ACTIONS,
    SEND_MAIL_ACTIONS,
    DETAILMAIL_ACTIONS,
} from '../actionTypes';


const requestSendMail = (request_data) => ({
    type: SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL,
    request_data,
});

const getListSend = () => ({
    type: GET_LIST_SEND_ACTIONS.REQUEST_GET_LIST_SEND,
});

const getDataMailFromDetail = (item) => ({
    type: DETAILMAIL_ACTIONS.PUSH_DETAILMAIL_TO_SENDMSG,
    item,
});
const uploadBase64Inbox = (base64code, filename) => ({
    type: SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64,
    base64code,
    filename,

});

const sendMailActions = {
    requestSendMail,
    getListSend,
    getDataMailFromDetail,
    uploadBase64Inbox,
};

export default sendMailActions;
