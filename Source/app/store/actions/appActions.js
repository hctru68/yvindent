import { APP_ACTIONS } from "../actionTypes";

const openSendMailPage = () => ({
   type:APP_ACTIONS.SEND_MAIL
});

const appAction = {
    openSendMailPage,
};

export default appAction;
