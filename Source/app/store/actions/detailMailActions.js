import {
    DETAILMAIL_ACTIONS,
} from '../actionTypes';


const getDetailMailByInboxId = (inboxId) => ({
    type: DETAILMAIL_ACTIONS.REQUEST_DETAILMAIL_GET,
    inboxId,
});
const moveDataToSendMail = (item) => ({
    type: DETAILMAIL_ACTIONS.PUSH_DETAILMAIL_TO_SENDMSG,
    item,
});
const updateIsReadMail = (messageId) => ({
    type: DETAILMAIL_ACTIONS.PUT_ISREAD_DETAILMAIL,
    messageId,
});

const detailMailActions = {
    getDetailMailByInboxId,
    moveDataToSendMail,
    updateIsReadMail,
};

export default detailMailActions;
