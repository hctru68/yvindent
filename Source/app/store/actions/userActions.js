import {
  USER_ACTIONS,
} from '../actionTypes';

const checkUserExist = () => ({ type: USER_ACTIONS.REQUEST_CHECK_EXISTED_USER });

const requestLogin = (username, password) => ({
  type: USER_ACTIONS.REQUEST_LOGIN,
  username,
  password,
});

const requestLogout = () => ({
  type: USER_ACTIONS.REQUEST_LOGOUT,
});

const requestUserUpdate = userinfo => ({
  type: USER_ACTIONS.REQUEST_USER_UPDATE,
  userinfo,
});

const userActions = {
  checkUserExist,
  requestLogin,
  requestLogout,
  requestUserUpdate,
};

export default userActions;
