import {
  CALENDAR_ACTIONS,
} from '../actionTypes';

const requestCalendar = () => ({
  type: CALENDAR_ACTIONS.REQUEST_CALENDAR,
});

const calendarAction = {
  requestCalendar,
};

export default calendarAction;
