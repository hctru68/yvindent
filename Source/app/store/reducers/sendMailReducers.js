import sendMailState from '../../common/objects/sendMailStateObject';
import {DETAILMAIL_ACTIONS, SEND_MAIL_ACTIONS} from '../actionTypes';

function handleSendMail(state = sendMailState.initialData(), action) {
    switch (action.type) {
        case DETAILMAIL_ACTIONS.PUSH_DETAILMAIL_TO_SENDMSG:
            return {
                ...state,
                sendMail: action.item,
            };
        case SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_SUCCESS:
        {
            return {
                ...state,
                ...action.uploadedData,
            };
        }
        case SEND_MAIL_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64:
        {
            return {
                ...state,
                ...action.uploadedData,
            };
        }
        default:
            return state;
    }
}

export default handleSendMail;
