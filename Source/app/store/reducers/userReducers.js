import userState from '../../common/objects/userStateObject';
import { USER_ACTIONS } from '../actionTypes';

function handleLogin(state = userState.initialData(), action) {
  // console.log(action);
  switch (action.type) {
    case USER_ACTIONS.REQUEST_LOGIN_SUCCESS:
      return {
        ...state,
        ...action.data,
      };
    case USER_ACTIONS.REQUEST_USER_UPDATE_SUCCESS:
      return {
        ...state,
        ...action.userData,
      };
    default:
      return state;
  }
}

export default handleLogin;
