import profileState from '../../common/objects/profileStateObject';
import { PROFILE_ACTIONS } from '../actionTypes';

function handleProfile(state = profileState.initialData(), action) {
    //console.log("Profile Reducers: ");
    // console.log(action);
    switch (action.type) {
        case PROFILE_ACTIONS.REQUEST_PROFILE_GETALL_SUCCESS:
            return {
                ...state,
                ...action.profileData,
            };
        case PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_SUCCESS:
        case PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_FAILURE:
            {
                return {
                    ...state,
                    ...action.updateData,
                };
            }
        case PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_SUCCESS:
            {
                return {
                    ...state,
                    ...action.uploadedData,
                };
            }
        case PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64:
            {
                //console.log("Profile Reducers: REQUEST_PROFILE_UPLOAD_BASE64");
                // console.log(action);
                return {
                    ...state,
                    //...action.uploadedData,
                };
            }
        default:
            return state;
    }
}

export default handleProfile;
