import {
    combineReducers,
} from 'redux';
import { reducer as formReducer } from 'redux-form';
import apps from './appReducers';
import users from './userReducers';
import inbox from './inboxReducers';
import profile from './profileReducers';
import calendar from './calendarReducers';
import detailMail from './detailMailReducers';
import sendMail from './sendMailReducers';

export default combineReducers({
    // add reducers component
    apps,
    calendar,
    users,
    inbox,
    detailMail,
    sendMail,
    profile,
    form: formReducer,
});
