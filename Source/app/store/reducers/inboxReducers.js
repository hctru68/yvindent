import inboxState from '../../common/objects/inboxStateObject';
import {GET_LIST_SEND_ACTIONS, INBOX_ACTIONS } from '../actionTypes';

function handleInbox(state = inboxState.initialData(), action) {
    switch (action.type) {
        case INBOX_ACTIONS.REQUEST_INBOX_GETALL_SUCCESS:
            return {
                ...state,
                ...action.inboxData,
            };
        case INBOX_ACTIONS.REQUEST_INBOX_DELETEBYID_SUCCESS:
            return {
                ...state,
                ...action.stateDelete,
            };
        case GET_LIST_SEND_ACTIONS.REQUEST_GET_LIST_SEND_SUCCESS:
            return {
                ...state,
                listSend: action.result,
            };
        default:
            return state;
    }
}

export default handleInbox;
