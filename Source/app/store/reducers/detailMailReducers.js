import detailState from '../../common/objects/detailMailStateObject';
import { DETAILMAIL_ACTIONS } from '../actionTypes';

function handleDetailMail(state = detailState.initialData(), action) {
    switch (action.type) {
        case DETAILMAIL_ACTIONS.REQUEST_DETAILMAIL_GET:
            return {
                ...state,
                detailMail: action.inboxItem,
            };
        default:
            return state;
    }
}

export default handleDetailMail;
