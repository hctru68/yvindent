import calendarStateObject from '../../common/objects/calendarStateObject';
import {
  CALENDAR_ACTIONS,
} from '../actionTypes';

function handleLogin(state = calendarStateObject.initialData(), action) {
  // console.log(action);
  switch (action.type) {
    case CALENDAR_ACTIONS.REQUEST_CALENDAR_SUCCESS:
      return {
        ...state,
        ...action.calendarData,
      };
    default:
      return state;
  }
}

export default handleLogin;
