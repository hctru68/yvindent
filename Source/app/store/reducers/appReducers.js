import appState from '../../common/objects/appStateObject';
import {APP_ACTIONS, USER_ACTIONS, INBOX_ACTIONS,PROFILE_ACTIONS, SEND_MAIL_ACTIONS} from '../actionTypes';

function handleApplication(state = appState.initialData(), action) {
  switch (action.type) {
    case APP_ACTIONS.HOME_SCREEN:
    case APP_ACTIONS.LOGIN_SCREEN:
    case APP_ACTIONS.SEND_MAIL:
      return {
        ...state,
        ...appState.updateNewPage(action.type),
      };
      case USER_ACTIONS.REQUEST_LOGIN:
      case INBOX_ACTIONS.REQUEST_INBOX_GETALL:
      return {
        ...state,
        ...appState.enableRequesting(action.type),
      };
      case USER_ACTIONS.REQUEST_LOGIN_FAILURE:
      case INBOX_ACTIONS.REQUEST_INBOX_GETALL_FAILURE:
      return {
        ...state,
        ...appState.setRequestError(action.errorMessage, action.type),
          };
      //case PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_FAILURE:
      // return {
      //        ...state,
      //        ...appState.setRequestUpdateProfile(action.type, action.result.msg, action.result.status),
      // };
      case USER_ACTIONS.REQUEST_LOGIN_SUCCESS:
      case INBOX_ACTIONS.REQUEST_INBOX_GETALL_SUCCESS:
      return {
        ...state,
        ...appState.setRequestSuccess(action.type),
      };
      //case PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE_SUCCESS:
      //    return {
      //        ...state,//giu lai param old
      //        ...appState.setRequestUpdateProfile(action.type, action.result.msg, action.result.status),
      //};

      case PROFILE_ACTIONS.REQUEST_PROFILE_UPDATE:
          return {
              ...state,
              status: '',
              msg: '',
          };

      //upload base64 
      case PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64:
          return {
              ...state,
              status: '',
          };
      case PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_SUCCESS:
          return {
              ...state,
              ...appState.setRequestUploadBase64(action.type, action.result.status, action.result.filename),
          };

      case PROFILE_ACTIONS.REQUEST_PROFILE_UPLOAD_BASE64_FAILURE:
          return {
              ...state,
              ...appState.setRequestUploadBase64(action.type, action.result.status, action.result.filename),
          };

       //Send mail
      case SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL_SUCCESS:
          return {
              ...state,
              ...appState.setRequestSuccess(action.type),
          };
      case SEND_MAIL_ACTIONS.REQUEST_SEND_MAIL_FAILURE:
          return {
              ...state,
              ...appState.setRequestError(action.errorMessage, action.type),
          };

      //upload base64
      case PROFILE_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_SUCCESS:
          return {
              ...state,
              status: '',
          };
      case PROFILE_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_SUCCESS:
          return {
              ...state,
              ...appState.setRequestUploadBase64(action.type, action.result.status, action.result.filename),
          };

      case PROFILE_ACTIONS.REQUEST_INBOX_UPLOAD_BASE64_FAILURE:
          return {
              ...state,
              ...appState.setRequestUploadBase64(action.type, action.result.status, action.result.filename),
          };
    default:
      return state;
  }
}

export default handleApplication;
