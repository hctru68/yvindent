import React from 'react';
import { Linking } from 'react-native';
import {
    createStackNavigator,
    createBottomTabNavigator,
} from 'react-navigation';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import AppNavigatorService from '../../services/navigator/AppNavigatorService';

import SplashScreen from '../splash-screen';
import Login from '../login';
import Profile from '../profile';
import Inbox from '../inbox';
import DetailMail from '../inbox/detailmail';
import SendMailPage from '../inbox/sendmail/sendmail';
import Calendar from '../calendar';
import Link from '../link';
import { APP_BACKGROUND_NAVI } from '../../common/constants/color';
import Size from '../../common/constants/size';

// Init navigation slack for app
const InboxNavigator = createStackNavigator({
    Inbox: {
        screen: Inbox,
    },
    SendMailPage: {
        screen: SendMailPage,
    },
    DetailMail: {
        screen: DetailMail,
    },
}, {
        headerMode: 'none',
        navigationOptions: {
            gesturesEnabled: false,
        },
    });

const TabNavigator = createBottomTabNavigator({
    Email: {
        screen: InboxNavigator,
    },
    Calendar,
    Link,
    Profile,
}, {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Profile') {
                    iconName = 'user-circle-o';
                } else if (routeName === 'Email') {
                    iconName = 'envelope-o';
                } else if (routeName === 'Calendar') {
                    iconName = 'calendar';
                } else if (routeName === 'Link') {
                    iconName = 'link';
                    console.log('FOCUS: ', focused);
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return (<FontAwesome name={iconName} size={Size.tabIconSize} color={tintColor} />);
            },
            tabBarOnPress: ({ navigation }) => {
                console.log('Navigation: ', navigation);
                const { routeName, key } = navigation.state;
                if (routeName === 'Link') {
                    const url = 'http://school.misd.ac/misd/misd/Login';
                    Linking.canOpenURL(url).then((supported) => {
                        if (supported) {
                            Linking.openURL(url);
                        } else {
                            console.log('Don\'t know how to open URI: ', url);
                        }
                    });
                } else {
                    navigation.navigate(key);
                }
            },
        }),
        tabBarOptions: {
            activeTintColor: APP_BACKGROUND_NAVI,
            inactiveTintColor: '#FDA94955',
            style: {
                height: Size.tabHeight,
            },
            labelStyle: {
                marginLeft: 0,
                textAlign: 'center',
                fontWeight: 'bold',
            },
            tabStyle: {
                height: Size.tabHeight,
                flexDirection: 'column',
                justifyContent: 'center',
            },
        },
    });

const MainNavigator = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen,
    },
    Login: {
        screen: Login,
    },
    Home: {
        screen: TabNavigator,
    },
}, {
        headerMode: 'none',
        navigationOptions: {
            gesturesEnabled: false,
        },
    });

export default MainNavigator;
