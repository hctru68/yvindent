import ImageConstant from '../../common/constants/image';
import { APP } from '../../store/actionTypes';
import I18n from '../../common/utils/languageHelper';

const TabList = [
  {
    id: APP.TICKET_PAGE,
    name: I18n.t('tickets'),
    iconSource: ImageConstant.TicketIcon,
  },
  {
    id: APP.KIOSK_PAGE,
    name: I18n.t('kiosk'),
    iconSource: ImageConstant.PopcornIcon,
  },
  {
    id: APP.SETTING_PAGE,
    name: I18n.t('setting'),
    iconSource: ImageConstant.SettingIcon,
  },
];

export default TabList;
