import React, { Component } from 'react';
import { Text } from 'react-native';
import {
  SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import styles from './styles';
import userActions from '../../store/actions/userActions';

import I18n from '../../common/utils/languageHelper';
import Size from '../../common/constants/size';

class Link extends Component {
  constructor(props) {
    super(props);
    this.actions = null;
    this.state = {
    };
  }

  componentDidMount() {
    this.mapVariableToProp();
  }

  mapVariableToProp = () => {
    const {
      actions,
    } = this.props;
    this.actions = actions;
  }

  render() {
    console.log('Link');

    return (
      <SafeAreaView style={styles.container}>
        <Text style={{ fontSize: 30, textAlign: 'center', width: Size.width }}>{I18n.t('link')}</Text>
      </SafeAreaView>
    );
  }
}

Link.propTypes = {
  actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
  const { apps } = state;
  return {
    apps,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...userActions,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);
