import { StyleSheet } from 'react-native';
import { APP_BACKGROUND } from '../../common/constants/color';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: APP_BACKGROUND,
  },
});

export default styles;
