import { StyleSheet } from 'react-native';
import { APP_BACKGROUND, APP_BACKGROUND_CONTENT, APP_BACKGROUND_2 } from '../../common/constants/color';
import Size from '../../common/constants/size';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: APP_BACKGROUND_CONTENT,
  },
  item: {
    flex: 1,
    backgroundColor: APP_BACKGROUND_CONTENT,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
    width: '100%',
  },
  emptyDate: {
    flex: 1,
    width: '100%',
    height: 'auto',
    paddingTop: 30,
  },
});

export default styles;
