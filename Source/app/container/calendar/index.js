import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';
import {
  SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Agenda } from 'react-native-calendars';

import generalStyle from '../general/general-style';
import styles from './styles';
import calendarActions from '../../store/actions/calendarActions';

import I18n from '../../common/utils/languageHelper';
import { objectIsEmpty } from '../../common/utils/validationHelper';
import calendarTheme from './calendar-theme';


class Calendar extends Component {
  constructor(props) {
    super(props);
    this.actions = null;
    this.state = {
      items: {},
      dateSelected: moment(new Date()).format('YYYY-MM-DD'),
      data: {},
    };
  }

  componentDidMount() {
    this.mapVariableToProp();
    this.actions.requestCalendar();
  }

  componentDidUpdate(prevProps) {
    const { calendar } = this.props;
    this.handlerCalendarChanged(prevProps.calendar, calendar);
  }

  handlerCalendarChanged = (prev, current) => {
    if (prev.data !== current.data && !objectIsEmpty(current.data)) {
      this.setState({ data: current.data }, () => {
        const { dateSelected } = this.state;
        this.getDataFromMonth(dateSelected);
      });
    }
  }

  mapVariableToProp = () => {
    const {
      actions,
    } = this.props;
    this.actions = actions;
  }

  getDataFromMonth = (day) => {
    const { items } = this.state;
    const { calendar } = this.props;
    const { data } = calendar;
    console.log('DAY: ', day, '---DATA: ', Object.keys(data));
    for (let i = -50; i < 50; i += 1) {
      const time = moment(day).valueOf() + i * 86400000;
      const strTime = moment(time).format('YYYY-MM-DD');
      items[strTime] = [];
      if (data[strTime]) {
        items[strTime] = data[strTime];
      }
    }
    const newItem = {};
    Object.keys(items).forEach((key) => {
      newItem[key] = items[key];
    });
    this.setState({
      items: newItem,
    });
    console.log('GET DATE MONTH: ', items);
  }

  // ==================== RENDER LAYOUT ========================
  renderItem = (item) => {
    return (
      <View style={styles.item}>
        <Text>{item.Name}</Text>
      </View>
    );
  };

  renderEmptyDate = () => (
    <View style={styles.item} />
  );

  rowHasChanged = (r1, r2) => {
    return r1.Name !== r2.Name;
  }

  render() {
    const {
      items,
      dateSelected,
    } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <Agenda
          loadItemsForMonth={this.getDataFromMonth}
          items={items}
          selected={dateSelected}
          renderItem={this.renderItem}
          renderEmptyDate={this.renderEmptyDate}
          rowHasChanged={this.rowHasChanged}
          theme={calendarTheme}
        />
        <View
          style={generalStyle.backgroundBelow}
        />
      </SafeAreaView>
    );
  }
}

Calendar.propTypes = {
  actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
  const { apps, calendar } = state;
  return {
    apps,
    calendar,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...calendarActions,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
