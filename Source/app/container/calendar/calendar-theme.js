import { APP_BACKGROUND, APP_BACKGROUND_CONTENT } from '../../common/constants/color';

const calendarTheme = {
  selectedDayBackgroundColor: APP_BACKGROUND,
  dotColor: APP_BACKGROUND,
  selectedDotColor: APP_BACKGROUND_CONTENT,
  textSectionTitleColor: APP_BACKGROUND,
  agendaKnobColor: APP_BACKGROUND,
  agendaTodayColor: APP_BACKGROUND,
};

export default calendarTheme;
