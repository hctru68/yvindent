import { StyleSheet } from 'react-native';
import Size from '../../common/constants/size';
import { APP_BACKGROUND } from '../../common/constants/color';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  generalHeader: {

  },
  backgroundBelow: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: Size.width,
    height: Size.height,
    zIndex: -10000,
    backgroundColor: APP_BACKGROUND,
  },
});

export default styles;
