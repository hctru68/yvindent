﻿import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    TextInput,
    ScrollView,
    Image,
    Alert,
    Platform,
} from 'react-native';
import {
    SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import FontAwesome from '@expo/vector-icons/FontAwesome';

import styles from './styles';
import generalStyle from '../general/general-style';
import userActions from '../../store/actions/userActions';
import profileActions from '../../store/actions/profileActions';
import ImageConstant from '../../common/constants/image';
import I18n from '../../common/utils/languageHelper';
import Size from '../../common/constants/size';

import { API_GETALL_PROFILE, API_URL_AVATAR, API_DOMAIN} from '../../common/constants/url';
import stylesProfile from './styleProfileForm';
import appAction from '../../store/actions/appActions';
import moment from 'moment';
import { ImagePicker, FileSystem, Constants, Permissions} from 'expo';
import base64 from 'base64-js';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.actions = null;
        this.state = {
            avatarBase64: null,
            password_old: '',
            password_confirm: '',
            password_new:'',
            year: '',
            lastName: '',
            birthday: '',
            address: '',
            email: '',
            gender: '',
            mobilePhone: '',
            program: '',
            telephone: '',
            usercode: '',
        };
    }

    componentDidMount() {
        this.mapVariableToProp();
        this.actions.getAllProfile();
    }

    componentDidUpdate(preProps) {
        const { apps } = this.props;
        //console.log("componentDidUpdate: ");
        //console.log(this.props);
        this.handlerLoadProfile(preProps.profile, this.props.profile);
        this.handlerUpdateProfile(preProps.profile, this.props.profile);
        this.handlerUploadBase64(preProps.profile, this.props.profile);
    }

    handlerUpdateProfile = (old, current) => {
        if (old.resultUpdateProfile != current.resultUpdateProfile && current.resultUpdateProfile.status != "") {
            console.log("refesh Profile: ");
            this.actions.getAllProfile();
            this.showAlerMessage(current.resultUpdateProfile.status, current.resultUpdateProfile.msg);
        }
    }

    handlerUploadBase64 = (old, current) => {
        if (old.resultUploadBase64 !== current.resultUploadBase64) {
            let resultUploadBase64 = current.resultUploadBase64 != null ? current.resultUploadBase64 : null;
            if (resultUploadBase64) {
                if (resultUploadBase64.status == 'success') {
                    const { password_new, password_confirm } = this.state
                    console.log("index.js -> handlerUploadBase64: ");
                    console.log(resultUploadBase64);

                    this.actions.updateProfile(password_new, password_confirm, resultUploadBase64.fileName);

                } else {
                    this.showAlerMessage('Upload image fail', resultUploadBase64.msg);
                }
            }

        }
    }

    showAlerMessage = (title, message) => {
        Alert.alert(title, message, [{
            text: 'OK',
            onPress: () => {
            },
        }]);
    }

    handlerLoadProfile = (old, current) => {
        if (old !== current) {
            let data = current.listProfile != null ? current.listProfile.data : null;
            console.log("Update UI Profile: ");
            console.log(data);
            if (data != null) {
                let birthdayFormat = '';
                if (data.Birthday != null && data.Birthday != "") {
                    const birthdayTimeUnit = parseFloat(data.Birthday.replace('/', '').replace('Date', '').replace('(', '').replace(')', ''));
                    let birthdayDate = moment(birthdayTimeUnit);
                    birthdayFormat = moment(birthdayDate).format('YYYY-MM-DD');
                }

                //AVATAR
                let avatar = data.Avatar != null ? data.Avatar : null;
                let imgOnline = { uri: '' };
                if (avatar != null && avatar != '') {
                    imgOnline.uri = API_URL_AVATAR + '/' + avatar;
                    iconAvatar = imgOnline;
                } else {
                    iconAvatar = (data.Gender == 1) ? ImageConstant.IconMale : ImageConstant.IconFeMale;
                }

                this.setState({
                    year: data.Year,
                    avatar: iconAvatar,
                    lastName: data.LastName,
                    firstName: data.FirstName,
                    birthday: birthdayFormat,
                    address: data.Address,
                    email: data.Email,
                    gender: data.Gender == 1 ? 'Nam' : 'Nữ',
                    mobilePhone: data.MobilePhone,
                    program: data.Program,
                    telephone: data.Telephone,
                    usercode: data.UserId, 
                    district: data.District,
                    city: data.City,
                    country: data.Country,
                    englishName: data.EnglishName
                });
            }
        }
    }

    mapVariableToProp = () => {
        const {
            actions,
        } = this.props;
        this.actions = actions;
    }

    onLogoutClick = () => {
        this.actions.requestLogout();
    }

    onImage = () => {

    }

    onSave = () => {
        const { password_new, password_confirm, avatarBase64, avatar } = this.state;
        if (avatarBase64 != null) {
            let image = avatar.uri;
            let filename = image.substring(image.lastIndexOf('/') + 1);
            console.log("Save:");
            console.log("actions.uploadBase64: " + filename);
            this.actions.uploadBase64(avatarBase64, filename);

            this.setState({
                avatarBase64: null,
            });

        } else {
            this.actions.updateProfile(password_new, password_confirm, "");
        }
        
    }

    _pickImage = async () => {
        // const permissions = 'Permissions.CAMERA_ROLL';
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        console.log('PERMISSION: ', status);
        if (status === 'granted') {
            const result = await ImagePicker.launchImageLibraryAsync({
                base64: true,
                allowsEditing: false,
                aspect: [4, 3],
            });

            if (!result.cancelled) {
                let imgOnline = { uri: '' };
                imgOnline.uri = result.uri;
                this.setState({
                    avatar: imgOnline,
                    avatarBase64: result.base64,
                });
            }
        }
    };

    render() {
        let { image } = this.state;
        return (
            <SafeAreaView style={stylesProfile.container}>
                <View style={[stylesProfile.navBar]}>
                    <Text style={{ width: '30%' }}></Text>
                    <Text style={{ width: '40%', color: '#fff', fontSize: Size.title, textAlign: 'center' }}>{I18n.t('profile')}</Text>
                    <TouchableOpacity
                        style={{
                            width: '15%',
                            height: Size.width * 0.1,
                        }}
                        onPress={this.onSave}>
                        <Text style={{ paddingTop: 5, flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>
                            <FontAwesome
                                name="check"
                                color="#fff"
                                size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            width: '15%',
                            height: Size.width * 0.1,
                        }}
                        onPress={this.onLogoutClick}>
                        <Text style={{ paddingTop: Platform.OS === 'ios' ? 5 : 10, flex: 1, textAlign: 'center', textAlignVertical: 'center' }}>
                            <FontAwesome
                                name="sign-out"
                                color="#fff"
                                size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                </View>
                <ScrollView style={{ width: '100%' }}>
                    <View style={stylesProfile.containerInput}>
                        <View style={stylesProfile.rowContainer}>
                            <TouchableOpacity onPress={this._pickImage}>
                                <Image
                                    resizeMode='cover'
                                    borderRadius={75}
                                    source={this.state.avatar
                                    }
                                    style={{ width: 150, height: 150, marginTop:10 }}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('password_new')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputField} value={this.state.password_new} underlineColorAndroid="transparent"
                                    onChangeText={(password_new) => { this.setState({ password_new }); }}
                                    secureTextEntry={true}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('password_confirm')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputField} value={this.state.password_confirm} underlineColorAndroid="transparent"
                                    onChangeText={(password_confirm) => { this.setState({ password_confirm }); }}
                                    secureTextEntry={true}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('firstName')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.firstName} underlineColorAndroid="transparent"
                                    onChangeText={(firstName) => { this.setState({ firstName }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('lastName')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput
                                    style={stylesProfile.textInputFieldReadOnly}
                                    underlineColorAndroid="transparent"
                                    value={this.state.lastName}
                                    onChangeText={(lastName) => { this.setState({ lastName }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('email')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.email} underlineColorAndroid="transparent"
                                    onChangeText={(email) => { this.setState({ email }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('mobilePhone')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.mobilePhone} underlineColorAndroid="transparent"
                                    onChangeText={(mobilePhone) => { this.setState({ mobilePhone }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('address')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.address} underlineColorAndroid="transparent"
                                    onChangeText={(address) => { this.setState({ address }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>   

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('district')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.district} underlineColorAndroid="transparent"
                                    onChangeText={(district) => { this.setState({ district }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('city')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.city} underlineColorAndroid="transparent"
                                    onChangeText={(city) => { this.setState({ city }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('country')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.country} underlineColorAndroid="transparent"
                                    onChangeText={(country) => { this.setState({ country }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('year')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={`${this.state.year}`} underlineColorAndroid="transparent"
                                    onChangeText={(year) => { this.setState({ year }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>

                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('birthday')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.birthday} underlineColorAndroid="transparent"
                                    onChangeText={(birthday) => { this.setState({ birthday }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('usercode')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={`${this.state.usercode}`} underlineColorAndroid="transparent"
                                    onChangeText={(usercode) => { this.setState({ usercode }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('program')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.program} underlineColorAndroid="transparent"
                                    onChangeText={(program) => { this.setState({ program }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                       
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('gender')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.gender} underlineColorAndroid="transparent"
                                    onChangeText={(gender) => { this.setState({ gender }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('telephone')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.telephone} underlineColorAndroid="transparent"
                                    onChangeText={(telephone) => { this.setState({ telephone }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('englishName')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.englishName} underlineColorAndroid="transparent"
                                    onChangeText={(englishName) => { this.setState({ englishName }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        {/*
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('ethnic')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputFieldReadOnly} value={this.state.ethnic} underlineColorAndroid="transparent"
                                    onChangeText={(ethnic) => { this.setState({ ethnic }); }}
                                    editable={false} selectTextOnFocus={false}
                                />
                            </View>
                        </View>
                        
                        <View style={stylesProfile.rowContainer}>
                            <Text style={stylesProfile.labelField}>{I18n.t('password_old')}</Text>
                            <View style={stylesProfile.marginVertical}>
                                <TextInput style={stylesProfile.textInputField} value={this.state.password_old} underlineColorAndroid="transparent"
                                    onChangeText={(password_old) => { this.setState({ password_old }); }}
                                    secureTextEntry={true}
                                />
                            </View>
                        </View>
                        */}

                        

                    </View>
                </ScrollView>
                <View
                    style={generalStyle.backgroundBelow}
                />
            </SafeAreaView>
        );
    }
}

Profile.propTypes = {
    actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
    const { apps, profile } = state;
    return {
        apps,
        profile,
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...appAction,
        ...userActions,
        ...profileActions,
        ...userActions,
    }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
