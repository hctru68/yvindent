import { StyleSheet, Platform } from 'react-native';
import { APP_BACKGROUND, INPUT_FIELD_BACKGROUND, APP_BACKGROUND_CONTENT } from '../../common/constants/color';
import Size from '../../common/constants/size';

const styleProfile = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: APP_BACKGROUND_CONTENT,
  },
  navBar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: Platform.OS === 'ios' ? 0 : 20,
    // height: 40,
    // flexDirection: 'row',
    // marginTop: 20,
    // backgroundColor: '#fbaf3f',
    // justifyContent: 'center',
    // alignItems: 'center',
  },

  inputStyle: {
    borderBottomColor: APP_BACKGROUND,
  },
  inputLabel: {
    color: APP_BACKGROUND,
  },
  input: {
    color: APP_BACKGROUND,
  },
  iconContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '20%',
    height: Size.inputFieldHeight,
  },
  containerInput: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: APP_BACKGROUND_CONTENT,
  },
  rowContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      alignItems: 'center',
  },
  labelField: {
      //fontSize: Size.inputTextSize,
      height: Size.inputFieldHeight,
      width: '35%',
      textAlign: 'right',
      
      paddingRight: 10,
      color: '#5c5c5c',
      textAlignVertical: 'bottom',
      ...Platform.select({
          ios: {
              paddingTop: 15,
          },
          android: {
              paddingBottom: 15,
          },
      }),
  },

  textInputField: {
      height: 30,
      borderBottomColor: '#bfbfbf',
      borderBottomWidth: 1,
      textAlignVertical: 'bottom',
      //backgroundColor: 'blue',
      width: '100%',
      ...Platform.select({
          ios: {
              paddingTop: 10,
          },
          android: {
              paddingBottom: 2,
          },
      }),
  },

  textInputFieldReadOnly: {
      color: '#999',
      height: 30,
      borderBottomColor: '#bfbfbf',
      borderBottomWidth:1,
      
      textAlignVertical: 'bottom',
      width: '100%',
      ...Platform.select({
          ios: {
              paddingTop: 10,
          },
          android: {
              paddingBottom: 2,
          },
      }),
  },

  marginVertical: {
    //flexDirection: 'row',
    //borderRadius: 8,
    //backgroundColor: INPUT_FIELD_BACKGROUND,
      width: '60%',
    //height: Size.inputFieldHeight,
    //marginBottom: Size.verticalSpacingLogin,
    //borderColor: '#9e9f9f',
    //borderStyle: 'solid',
    //borderWidth: 0.2,
  },
});

export default styleProfile;
