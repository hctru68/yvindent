import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity, Image, Platform, Alert } from 'react-native';
import Swipeout from 'react-native-swipeout';
import {
    SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import FontAwesome from '@expo/vector-icons/FontAwesome';

import styles from './styles';
import generalStyle from '../general/general-style';
import stylesProfile from '../profile/styleProfileForm';
import userActions from '../../store/actions/userActions';
import inboxActions from '../../store/actions/inboxActions';

import I18n from '../../common/utils/languageHelper';
import Size from '../../common/constants/size';

import { API_DOMAIN, API_GETALL_INBOX } from '../../common/constants/url';
import { APP_BACKGROUND, APP_BACKGROUND_2, APP_BACKGROUND_CONTENT } from '../../common/constants/color';
import ImageConstant from '../../common/constants/image';
import appAction from '../../store/actions/appActions';
import NavigatorService from '../../services/navigator/AppNavigatorService';
import Moment from 'moment';

class Inbox extends Component {
    constructor(props) {
        super(props);
        this.actions = null;
        this.state = {
            loading: false,
            page: 1,
            data: [],
            refreshing: false,
            endLoadMore: 0,
        };
    }
    async componentDidMount() {
        await this.mapVariableToProp();
        await this.actions.getAllInbox(this.state.page);
    }
    componentDidUpdate(preProps) {
        const {
            apps,
            inbox,
        } = this.props;
        this.handlerRequestFailure(preProps.apps, apps);
        this.handlerLoadMoreInbox(preProps.inbox, inbox);
    }
    handlerLoadMoreInbox = (old, current) => {
        if (old !== current) {
            this.setState({
                data: this.state.page === 1 ? current.listInbox.itemList : [...this.state.data, ...current.listInbox.itemList],
                loading: false,
                refreshing: false,
                endLoadMore: ((current.listInbox.curPage == current.listInbox.sumPage) ? 0 : 0.1)
            });
        }
    }
    handlerRequestFailure = (old, current) => {
        if (old.isRequesting !== current.isRequesting && !current.isRequesting) {
            this.setState({
                loading: false,
                refreshing: false
            });
        }
    }
    mapVariableToProp = () => {
        const {
            actions,
        } = this.props;
        this.actions = actions;
    }

    onDetailMailClick = (item) => {
        this.updateIsReadInList(item);
        this.actions.moveToDetailMail(item);
        NavigatorService.navigate('DetailMail');
    }
    updateIsReadInList = (item) => {
        let listInbox = this.state.data;
        if (listInbox && listInbox.length > 0) {
            for (var i = 0; i < listInbox.length; i++) {
                let pos = listInbox.indexOf(item);
                listInbox[pos].IsRead = true;
                if (pos > -1) {
                    this.setState(() => {
                        return { data: listInbox }
                    });
                    return;
                }
            }
        }
    }
    removeItemDeleted = (item) => {
        var result = [];
        if (this.state.data && this.state.data.length > 0) {
            result = this.state.data;
            var index = result.indexOf(item);
            result.splice(index, 1);
        }
        return result;
    }
    renderRowItem = ({ item, index }) => {
        let imgOnline = { uri: '' };
        let iconAvatar = (item.Avatar != null && item.Avatar != '') ? API_DOMAIN + '/UserFile/UserPhoto/' + item.Avatar :
            ((item.Gender == 'Nam') ? ImageConstant.IconMale : ImageConstant.IconFeMale);
        if (item.Avatar != null && item.Avatar != '') {
            imgOnline.uri = API_DOMAIN + '/UserFile/UserPhoto/' + item.Avatar;
            iconAvatar = imgOnline;
        }
        let createDate = '';
        if (item && item.CreateDate != null) {
            Moment.locale('en');
            let currentDate = new Date();
            let minToday = new Date();
            minToday.setDate(minToday.getDate() - 1);
            let strFormat = 'MMM-DD';
            if (currentDate > item.CreateDate && item.CreateDate > minToday) {
                strFormat = 'HH:mm';
            }
            createDate = Moment(item.CreateDate).format(strFormat);
        }
        let swipeBtns = [{
            text: 'Delete',
            backgroundColor: 'red',
            underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
            onPress: () => {
                //const deletingRow = this.state.data;
                Alert.alert(
                    'Delete',
                    'Are you sure you want to delete ?',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Yes', onPress: () => {
                                this.actions.deleteInboxById(item.MessageId);
                                this.setState({
                                    data: this.removeItemDeleted(item),
                                });
                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        }];
        return (
            <View>
                <Swipeout right={swipeBtns} autoClose='true' backgroundColor='transparent'>
                    <TouchableOpacity onPress={() => { this.onDetailMailClick(item) }}>
                        <View style={styles.itemInbox} >
                            <View style={styles.avatar}>
                                <Image resizeMode="contain" style={{ width: 40, height: 40, }}
                                    resizeMode='cover' borderRadius={20} source={iconAvatar} />
                            </View>
                            <View style={styles.subject}>
                                <Text style={[item.IsRead ? { fontWeight: 'normal' } : { fontWeight: 'bold' }]}>
                                    {item.Subject.substring(0, 36)}{item.Subject.length > 36 ? '...' : ''}
                                </Text>
                                <Text style={{ color: '#999999' }}>{item.SenderUserName}</Text>
                            </View>

                            <View style={styles.attachment}>
                                <Text style={{ fontSize: 9, color: '#999', marginBottom: 9, }}>{createDate}</Text>
                                <Text style={[(item.attachfile.length == 0) ? styles.attachmentHide : null]}>
                                    <FontAwesome name="paperclip" color="#999999" size={Size.buttonIconSize * 0.6} />
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Swipeout>
                <View style={styles.borderbt}></View>
            </View>
        );
    }

    handleRefresh = () => {
        this.setState({
            page: 1,
            refreshing: true,
        }, () => {
            this.actions.getAllInbox(this.state.page);
        })
    };
    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
            loading: false,
        }, () => {
            this.actions.getAllInbox(this.state.page);
        })
    }
    onSendMailClick = () => {
        this.actions.openSendMailPage();
    }

    render() {
        const { inbox } = this.props;
        let random = () => {
            return Math.random();
        }
        return (
            <SafeAreaView style={styles.container}>
                <View style={[stylesProfile.navBar]}>
                    <Text style={{ width: '20%' }}></Text>
                    <Text style={{ width: '60%', color: '#fff', fontSize: Size.title, textAlign: 'center' }}>{I18n.t('inbox')}</Text>
                    <TouchableOpacity
                        style={{ width: '20%', height: Size.width * 0.1, }}
                        onPress={this.onSendMailClick}
                    >
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center', paddingTop: 5 }}>
                            <FontAwesome name="pencil-square-o"
                                color="#fff" size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, backgroundColor: APP_BACKGROUND_CONTENT }}>
                    {
                        <FlatList
                            data={this.state.data}
                            keyExtractor={(index) => index.toString() + random()}
                            renderItem={this.renderRowItem}
                            refreshing={this.state.refreshing}
                            onRefresh={this.handleRefresh}
                            onEndReached={this.handleLoadMore}
                            onEndReachedThreshold={this.state.endLoadMore}
                        />
                    }
                </View>
                <View
                    style={generalStyle.backgroundBelow}
                />
            </SafeAreaView>
        );
    }
}

Inbox.propTypes = {
    actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
    const { apps, inbox } = state;
    return {
        apps,
        inbox,
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...appAction,
        ...inboxActions,
    }, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(Inbox);
