import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView, WebView, Linking, Platform } from 'react-native';
import {
    SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import FontAwesome from '@expo/vector-icons/FontAwesome';

import styles from './styles';
import generalStyle from '../../general/general-style';
import styleProfile from '../../profile/styleProfileForm';
import detailMailActions from '../../../store/actions/detailMailActions';

import I18n from '../../../common/utils/languageHelper';
import Size from '../../../common/constants/size';

import appAction from '../../../store/actions/appActions';
import { APP_BACKGROUND, APP_BACKGROUND_CONTENT } from '../../../common/constants/color';
import { API_DOMAIN } from '../../../common/constants/url';
import ImageConstant from '../../../common/constants/image';
import NavigatorService from '../../../services/navigator/AppNavigatorService';
import Moment from 'moment';

class DetailMail extends Component {
    constructor(props) {
        super(props);
        this.actions = null;
        this.state = {
            inboxId: 0,
        };
    }

    componentDidMount() {
        this.mapVariableToProp();
        const { detailMail } = this.props;
        if (detailMail && detailMail.detailMail) {
            console.log(detailMail.detailMail);
            this.actions.updateIsReadMail(detailMail.detailMail.MessageId);
        }
    }
    componentDidUpdate(preProps) {
        //const { apps } = this.props;
        //this.handlerRequestFailure(preProps.apps, apps);
    }
    mapVariableToProp = () => {
        const {
            actions,
        } = this.props;
        this.actions = actions;
    }

    onBackToInboxListClick = () => {
        NavigatorService.reset('Home');
    }
    onComposeMailClick = () => {
        //NavigatorService.navigate('DetailMail');
        const { detailMail } = this.props;
        if (detailMail && detailMail.detailMail != null) {
            this.actions.moveDataToSendMail(detailMail.detailMail);
        }
        this.actions.openSendMailPage();
    }
    handlerOpenFileAttachClick = (fileName) => {
        let url = API_DOMAIN + '/UserFile/HinhAnh/' + fileName;
        console.log(url);
        Linking.canOpenURL(url).then((supported) => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log('Don\'t know how to open URI: ', url);
                const {
                    onError,
                } = this.props;
                const message = `Don't know how to open URI: ${url}`;
                onError(message);
            }
        });
    }
    renderFileAttachLink = (item) => {
        let fileName = item.FileName;
        let ext = fileName.substring(fileName.lastIndexOf("."));
        let icon = 'file';
        //IMAGE
        if (ext == '.png' || ext == '.jpg' || ext == '.jpeg' || ext == '.gif' || ext == '.bmp' || ext == '.tiff') {
            icon = 'image';
        }
        //PDF
        else if (ext == '.pdf') {
            icon = 'file-pdf-o';
        }
        //EXCEL
        else if (ext == '.xls' || ext == '.xlsx') {
            icon = 'file-excel-o';
        }
        //EXCEL
        else if (ext == '.doc' || ext == '.docx') {
            icon = 'file-word-o';
        }
        //RAR, ZIP
        else if (ext == '.rar' || ext == '.zip') {
            icon = 'file-archive-o';
        }
        const view = (
            <Text style={styles.attachmentFile}>
                {/*<FontAwesome
                    name={icon}
                    color="#FDA949"
                    size={Size.buttonIconSize * 3}
                />
                {"\n"}*/}
                {item.FileName}
            </Text>
        );
        return view;
    }
    render() {
        const { detailMail } = this.props;
        //console.log('Detail Mail: ', detailMail);
        let subject = '';
        let message = '';
        let senderUserName = '';
        let createDate = '';
        let avatar = '';
        let gender = 'Nam';
        let attachFile = [];
        if (detailMail && detailMail.detailMail) {
            subject = detailMail.detailMail.Subject;
            message = detailMail.detailMail.Message;
            senderUserName = detailMail.detailMail.SenderUserName;
            createDate = detailMail.detailMail.CreateDate;
            avatar = detailMail.detailMail.Avatar;
            gender = detailMail.detailMail.Gender;
            attachFile = detailMail.detailMail.attachfile;
            if (createDate != null) {
                Moment.locale('en');
                createDate = Moment(createDate).format('YYYY-MM-DD HH:mm:ss')
            }
        }

        //AVATAR
        let imgOnline = { uri: '' };
        let iconAvatar = (avatar != null && avatar != '') ? API_DOMAIN + '/UserFile/UserPhoto/' + avatar :
            ((gender == 'Nam') ? ImageConstant.IconMale : ImageConstant.IconFeMale);
        if (avatar != null && avatar != '') {
            imgOnline.uri = API_DOMAIN + '/UserFile/UserPhoto/' + avatar;
            iconAvatar = imgOnline;
        }

        return (
            <SafeAreaView style={styles.container}>
                <View style={styleProfile.navBar}>
                    <View style={{ width: '9%', }}>
                        <TouchableOpacity onPress={this.onBackToInboxListClick}>
                            <Text style={{ textAlign: 'left', textAlignVertical: 'center', paddingTop: 7, paddingLeft: 3 }}>
                                <FontAwesome name="chevron-left"
                                    color="#FFFFFF" size={Size.buttonIconSize} />
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '91%' }}>
                        <Text style={styles.subjectTitle}>{subject}</Text>
                    </View>
                </View>
                <ScrollView style={styles.scrollViewHTML}>
                    <View style={{ padding: 3, }}>
                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ flex: 1.3, }}>
                                <Image resizeMode="contain" style={{ width: 36, height: 36, margin: 5 }}
                                    resizeMode='cover' borderRadius={18}
                                    source={iconAvatar} />
                            </View>
                            <View style={{ flex: 7, }}>
                                <Text style={{ color: "#999", marginBottom: 3, marginTop: 3, }}> Sender: {senderUserName}</Text>
                                <Text style={{ color: "#999", }}> Sent date: {createDate} </Text>
                            </View>
                            <View style={{ flex: 1.5, alignItems: 'center' }}>
                                <TouchableOpacity onPress={this.onComposeMailClick}>
                                    <Text style={styles.btnReply}>
                                        <FontAwesome name="reply" size={Size.buttonIconSize} />
                                        {"\n"}Reply
                                </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.detaiMessage}>
                            {
                                Platform.OS === 'ios' ? (
                                    <WebView
                                        scalesPageToFit={false}
                                        source={{ html: message }}
                                    />)
                                    : (<WebView source={{ html: message }} />)

                            }
                        </View>
                        <View style={styles.detailAttachments}>
                            {
                                attachFile.length > 0 ? attachFile.map((item, index) => (
                                    <TouchableOpacity key={index}
                                        onPress={() => { this.handlerOpenFileAttachClick(item.FileName) }} >
                                        {this.renderFileAttachLink(item)}
                                    </TouchableOpacity>
                                )) : null
                            }
                        </View>
                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between', padding: 10,
                        }}>

                        </View>
                    </View>
                </ScrollView>
                <View
                    style={generalStyle.backgroundBelow}
                />
            </SafeAreaView >
        );
    }
}

DetailMail.propTypes = {
    actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
    const { apps, detailMail } = state;
    return {
        apps,
        detailMail,
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...appAction,
        ...detailMailActions,
    }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailMail);
