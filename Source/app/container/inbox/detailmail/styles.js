import { StyleSheet } from 'react-native';
import { APP_BACKGROUND_CONTENT } from '../../../common/constants/color';
import Size from '../../../common/constants/size';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: APP_BACKGROUND_CONTENT,
    },
    detaiMessage: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#eee',
        //borderBottomWidth: 0,
        //shadowColor: '#000',
        //shadowOffset: { width: 0, height: 2 },
        //shadowOpacity: 0.5,
        //shadowRadius: 2,
        //elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        padding: 2,
        minHeight: 375,
    },
    detailAttachments: {
        flexDirection: 'column',
        flex: 1,
    },
    attachmentFile: {
        margin: 5,
        padding: 5,
        fontSize: Size.attachFile,
        borderWidth: 0.5,
        borderRadius: 2,
        borderColor: '#eee',
        backgroundColor: 'rgba(159, 170, 179, 0.05)',
    },
    btnReply: {
        color: '#666',    
        marginTop: 3,
    },
    scrollViewHTML: {
        width: '100%',
        backgroundColor: APP_BACKGROUND_CONTENT
    },
    subjectTitle: {
        fontSize: Size.titleMail,
        marginBottom: 3,
        color: 'white',
        paddingTop: 8,
    },
    borderbt: {
        borderWidth: 0.5,
        borderColor: '#eee',
        height: 1,
        marginTop: 3,
        marginBottom: 3,
    },
});
export default styles;
