import { StyleSheet } from 'react-native';
import { APP_BACKGROUND_CONTENT } from '../../common/constants/color';
import Size from '../../common/constants/size';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: APP_BACKGROUND_CONTENT,
    },
    itemInbox: {
        width: Size.width,
        height: 55,
        paddingVertical: 5,
        paddingLeft: 3,
        paddingRight: 10,
        flexDirection: 'row',       
    },
    avatar: {
        flex: 2,       
        marginRight: 5,
        marginTop: 4,
        alignItems:'center',
    },
    subject: {
        flex: 10,       
        height: 40,
    },
    attachmentHide: {       
        display: 'none',       
    },
    attachment: {
        flex: 1.2,       
        paddingTop: 3,        
        height: 40,
        alignItems:'center',
    },
    arrowRight: {
        flex: 1,        
        paddingTop: 11,   
        alignItems: 'flex-end',
        height: 40,
    },
    borderbt: {
        borderWidth: 0.5,
        borderColor: '#eee',
        height: 1,
        marginTop: 3,
        marginBottom: 3,
    },   
});

export default styles;
