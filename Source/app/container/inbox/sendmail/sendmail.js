/*eslint-disable*/
import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity,Keyboard,ScrollView } from 'react-native';
import FontAwesome from '@expo/vector-icons/FontAwesome';

import {
    SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import StyleSendMail from './StyleSendMail';
import generalStyle from '../../general/general-style'
import stylesProfile from '../../profile/styleProfileForm';
import Size from '../../../common/constants/size';
import sendMailActions from '../../../store/actions/sendMailAction';
import NavigatorService from "../../../services/navigator/AppNavigatorService";
import MultiSelect from 'react-native-multiple-select';
import { ImagePicker, Permissions} from 'expo';
import base64 from 'base64-js';
import {validateParamsRequired} from "../../../common/utils/validationHelper";


class SendMailPage extends Component {
    constructor(props) {
        super(props);
        const { sendMail } = this.props;
        let list_revceive = [];
        if (sendMail && sendMail.sendMail) {
            list_revceive.push(sendMail.sendMail.SenderUserId);
        }
        this.state = {
            listReceive: list_revceive,
            Message: '',
            Subject: '',
            photoList: [],
            items: [],
        }
        this.validateParams = this.validateParams.bind(this);
    }

    onSelectedItemsChange = listReceive => {
        this.setState({ listReceive });
    };
    validateParams() {
        const { listReceive, Message, Subject } = this.state;
        return validateParamsRequired([listReceive, Message, Subject]);
    }
    componentDidMount() {
        this.mapVariableToProp();
        this.actions.getListSend();

    }
    handlerUploadBase64 = (old, current) => {
        if (old.resultUploadBase64 !== current.resultUploadBase64) {
            let resultUploadBase64 = current.resultUploadBase64 != null ? current.resultUploadBase64 : null;
            if (resultUploadBase64) {
                if (resultUploadBase64.status == 'success') {
                    this.setState({
                        photoList: [...this.state.photoList,resultUploadBase64.fileName],
                    });
                } else {
                    this.showAlerMessage('Upload file fail', resultUploadBase64.msg);
                }
            }

        }
    }
    _pickImage = async () => {
        Keyboard.dismiss
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === 'granted') {
            const result = await ImagePicker.launchImageLibraryAsync({
                base64: true,
                allowsEditing: false,
                aspect: [4, 3],
            });

            if (!result.cancelled) {
                this.actions.uploadBase64Inbox(result.base64, result.uri.substring(result.uri.lastIndexOf('/') + 1));
                this.setState({
                    avatar: result.uri,
                    avatarBase64: result.base64,
                });
            }
        }
    };

    handleBackInbox() {
        NavigatorService.back();
    }
    handleSendMail = () => {
        if(this.state.listReceive.length > 0 && this.state.Message !== '' && this.state.Subject !== ''){
            this.actions.requestSendMail(this.state);
            NavigatorService.back();
        }
    }

    handleGetListSend = (old, current) => {
        if (old.listSend !== current.listSend) {
            const listSends = Object.keys(current.listSend).map(key => ({
                UserId: current.listSend[key].UserId,
                UserName: current.listSend[key].UserName,
                FullName: current.listSend[key].FullName + '('+ current.listSend[key].UserName +')' ,
            }));
            this.setState({
                items: listSends,
            });
        }
    }
    removeAttachment = (photo) => {
        this.setState({
             photoList: this.state.photoList.filter((x) => x !== photo)
        });
    }

    componentDidUpdate(preProps) {
        const { inbox,sendMail } = this.props;
        console.log('this is componentDidUpdate',sendMail);
        this.handleGetListSend(preProps.inbox, inbox);
        this.handlerUploadBase64(preProps.sendMail, sendMail);
    }
    mapVariableToProp = () => {
        const {
            actions,
        } = this.props;
        this.actions = actions;
    }

    render() {
        const { listReceive } = this.state;
        return (
            <SafeAreaView style={StyleSendMail.container} >
                <View style={stylesProfile.navBar}>
                    <TouchableOpacity
                        style={StyleSendMail.back}
                        onPress={this.handleBackInbox}>
                        <Text style={{ flex: 1, textAlign: 'left', textAlignVertical: 'center', paddingTop: 5, paddingLeft: 3  }}>
                            <FontAwesome
                                name="chevron-left"
                                color="#fff"
                                size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                    <View style={{ width: '10%' }}/>
                    <Text style={StyleSendMail.tile_create_email}></Text>
                    <TouchableOpacity
                        style={StyleSendMail.attachments}
                        onPress={this._pickImage}>

                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center', paddingTop: 5, }}>
                            <FontAwesome
                                name="paperclip"
                                color="#fff"
                                size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={StyleSendMail.send_mail}
                        onPress={this.handleSendMail}>
                        {/*disabled={!this.validateParams()}*/}
                        <Text style={{ flex: 1, textAlign: 'center', textAlignVertical: 'center', paddingTop: 5, }}>
                            <FontAwesome
                                name="send-o"
                                color="#fff"
                                size={Size.buttonIconSize}
                            />
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={StyleSendMail.sendMailContent} >
                    <Text style={StyleSendMail.title_to}>Send to </Text>
                    <View style={{ paddingTop: 10, width: '100%' }}>
                        <MultiSelect
                            hideTags
                            items={this.state.items}
                            uniqueKey="UserId"
                            ref={(component) => { this.multiSelect = component }}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={listReceive}
                            selectText="Email list"
                            searchInputPlaceholderText="Search Emails..."
                            onChangeInput={(text) => console.log(text)}
                            tagRemoveIconColor="#CCC"
                            tagBorderColor="#CCC"
                            tagTextColor="#CCC"
                            selectedItemTextColor="#fbaf3f"
                            selectedItemIconColor="#fbaf3f"
                            itemTextColor="#000"
                            itemFontSize={12}
                            displayKey="FullName"
                            searchInputStyle={{ color: '#CCC' }}
                            submitButtonColor="#fbaf3f"
                            submitButtonText="Select"
                        />
                    </View>

                    <Text style={StyleSendMail.title_subject}>Subject </Text>
                    <TextInput
                        style={StyleSendMail.input_subject}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        onChangeText={(Subject) => { this.setState({ Subject }); }}

                    />
                    <Text style={StyleSendMail.title_content}>Message</Text>
                    <TextInput
                        style={StyleSendMail.input_message}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        multiline={true}
                        numberOfLines={10}
                        editable={true}
                        onChangeText={(Message) => { this.setState({ Message }); }}
                    />
                    <ScrollView
                        style={StyleSendMail.contentContainer}
                        showsVerticalScrollIndicator={true}>
                            {this.state.photoList.map(photo => (
                                <View key={photo} style={{flex: 1,
                                    flexDirection: 'row',}}>
                                    <TouchableOpacity
                                        style={{flex:9}}>
                                        <Text> {photo} </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{flex:1}}
                                        onPress={()=> {this.removeAttachment(photo)}}>
                                        <FontAwesome
                                            name="remove"
                                            color="#ccc"
                                            size={Size.buttonIconSize}
                                        />
                                    </TouchableOpacity>
                                </View>
                            ))}
                    </ScrollView>
                </View>
                <View
                    style={generalStyle.backgroundBelow}
                />

            </SafeAreaView>

        );
    }
}


const mapStateToProps = (state) => {
    const { apps, inbox, sendMail } = state;
    return {
        apps,
        inbox,
        sendMail,

    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...sendMailActions,

    }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SendMailPage);
