import { StyleSheet } from 'react-native';
import Size from "../../../common/constants/size";
import { APP_BACKGROUND, INPUT_FIELD_BACKGROUND, APP_BACKGROUND_CONTENT } from '../../../common/constants/color';


const styleSendMail = StyleSheet.create({
    contentContainer: {
        flex:1,
        paddingVertical: 20,

    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: APP_BACKGROUND_CONTENT,
    },
    sendMailContent: {
        flex: 1,
        backgroundColor: 'white',
        width:'100%',
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
    },
    back: {
        width: '10%',
        height: Size.width * 0.1,
        flex: 2,
    },
    attachments:{
        width: '12%',
        height: Size.width * 0.1,
    },
    send_mail:{
        width: '12%',
        height: Size.width * 0.1,
    },
    tile_create_email: {
        width: '52%',
        color: '#fff',
        fontSize: Size.title_new_message,
        textAlign: 'center',
        textAlignVertical: 'center',
        height: Size.width * 0.1,
    },
    input_send_to:{
        height:30,
        // borderBottomColor:'#fbaf3f',
        paddingBottom:1
    },
    detailAttachments:{
        // backgroundColor:'red',
        height:80,
    },
    input_subject:{
        height: Size.subjectInput,
        paddingLeft: 5,
        //borderBottomColor:'#fbaf3f',
        //paddingBottom:8,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
    },
    input_message:{
        paddingLeft: 5,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        height: Size.messageInput,
        textAlignVertical: 'top',
    },
    selectbox:{
        width:100
    },
    title_to:{
        color: '#000'
    },
    title_subject:{
        color: '#000',

    },
    title_content:{
        color: '#000'
    }

});
export default styleSendMail;
