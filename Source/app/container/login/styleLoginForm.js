import { StyleSheet } from 'react-native';
import { APP_BACKGROUND, INPUT_FIELD_BACKGROUND } from '../../common/constants/color';
import Size from '../../common/constants/size';

const stylesLogin = StyleSheet.create({
  inputStyle: {
    borderBottomColor: APP_BACKGROUND,
  },
  inputLabel: {
    color: APP_BACKGROUND,
  },
  input: {
    color: APP_BACKGROUND,
  },
  iconContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '20%',
    height: Size.inputFieldHeight,
  },
  textInputField: {
    width: '80%',
    height: Size.inputFieldHeight,
    color: APP_BACKGROUND,
    fontSize: Size.inputTextSize,
  },
  marginVertical: {
    flexDirection: 'row',
    borderRadius: 8,
    backgroundColor: INPUT_FIELD_BACKGROUND,
    width: '100%',
    height: Size.inputFieldHeight,
    marginBottom: Size.verticalSpacingLogin,
    borderColor: '#9e9f9f',
    borderStyle: 'solid',
    borderWidth: 0.2,
  },
  loginButton: {
    marginTop: Size.verticalSpacingLogin,
    backgroundColor: APP_BACKGROUND,
    borderColor: 'transparent',
  },
  linkContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingLeft: '5%',
    paddingRight: '5%',
    height: Size.linkContainerHeight,
    marginBottom: Size.verticalSpacingLogin * 0.5,
  },
  linkStyle: {
    color: APP_BACKGROUND,
    fontSize: Size.linkFontSize,
  },
  linkContactContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: '10%',
    paddingRight: '10%',
  },
  contactLinkStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default stylesLogin;
