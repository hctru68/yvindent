import React, { Component } from 'react';
import {
  Alert,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {
  SafeAreaView,
} from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import BallIndicator from '../../components/ball-indicator';

import I18n from '../../common/utils/languageHelper';
import styles from './styles';
import userActions from '../../store/actions/userActions';

import LoginForm from './login-form';
import ImageConstant from '../../common/constants/image';
import commontStyles from '../../components/commonStyle';
import { WHITE_DARKEN_BACKGROUND } from '../../common/constants/color';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRequesting: false,
    };
  }

  componentDidMount() {
    this.mapVariableToProp(this.props);
  }

  componentDidUpdate(preProps) {
    const { apps } = this.props;
    this.handlerRequestFailure(preProps.apps, apps);
  }

  mapVariableToProp = () => {
    const {
      actions,
    } = this.props;
    this.actions = actions;
  }

  // ======================HANDLER EVENT ===================================
  onLoginRequest = (username, password) => {
    this.actions.requestLogin(username, password);
  }

  handlerErrorLoginForm = (message) => {
    this.showAlerMessage(I18n.t('error'), message);
  }

  handlerRequestFailure = (old, current) => {
    if (old.isRequesting !== current.isRequesting) {
      console.log('Update Requesting: ', current.isRequesting);

      this.setState({
        isRequesting: current.isRequesting,
      });
    }

    if (old.isFailure !== current.isFailure && current.isFailure) {
      this.showAlerMessage(I18n.t('error'), current.errorMessage);
    }
  }

  // ======================RENDER LAYOUT ===================================
  showAlerMessage = (title, message) => {
    Alert.alert(title, message, [{
      text: 'OK',
      onPress: () => {},
    }]);
  }

  renderHeader = () => (
    <View style={styles.containerHeader}>
      <Image
        resizeMode="contain"
        style={styles.logo}
        source={ImageConstant.Logo}
      />
      <Text style={styles.title}>{I18n.t('misd')}</Text>
    </View>
  );

  renderFormLogin = () => (
    <View style={styles.containerLogin}>
      <LoginForm
        style={styles.loginForm}
        onLoginRequest={this.onLoginRequest}
        onError={this.handlerErrorLoginForm}
      />
    </View>
  );

  render() {
    console.log('Login');
    const {
      isRequesting,
    } = this.state;

    return (
      <SafeAreaView
        forceInset={{ top: 'always' }}
        style={styles.container}
      >
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            scrollEnable={false}
            contentContainerStyle={styles.container}
          >
            {
              this.renderHeader()
            }
            {
              this.renderFormLogin()
            }
          </ScrollView>
        </KeyboardAvoidingView>
        <Image
          source={ImageConstant.BackgroundImage}
          style={styles.backgroundImage}
        />
        <View style={styles.layerHidden} />
        {
          isRequesting && (
            <View style={commontStyles.overlay}>
              <BallIndicator color={WHITE_DARKEN_BACKGROUND} />
            </View>
          )
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    apps,
    users,
  } = state;
  return {
    apps,
    users,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...userActions,
  }, dispatch),
});

Login.propTypes = {
  apps: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  actions: PropTypes.any, // eslint-disable-line
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
