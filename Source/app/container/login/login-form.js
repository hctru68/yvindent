import React, { Component } from 'react';
import {
  Linking,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { MailComposer } from 'expo';
import PropTypes from 'prop-types';
import FontAwesome from '@expo/vector-icons/FontAwesome';

import Button from 'apsl-react-native-button';

import { APP_BACKGROUND, APP_NAME } from '../../common/constants/color';

import I18n from '../../common/utils/languageHelper';
import stylesLogin from './styleLoginForm';
import Size from '../../common/constants/size';

const iconName = 'user-circle-o';
const passName = 'lock';
const mail = 'envelope-o';
const phone = 'phone';
const contactInfo = 'Info@misd.ac';
const phoneNumber = '(+84) 909289955';
const url = 'http://school.misd.ac';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  // =============HANDER ACTION=======================
  handlerLoginClick = () => {
    const { username, password } = this.state;
    const { onLoginRequest } = this.props;
    onLoginRequest(username, password);
  }

  handlerRegisterClick = () => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ', url);
        const {
          onError,
        } = this.props;
        const message = `Don't know how to open URI: ${url}`;
        onError(message);
      }
    });
  }

  handlerSendMail = () => {
    MailComposer.composeAsync({
      recipients: ['Info@misd.ac'],
      subject: 'Need Support from MISD',
    }).then((status) => {
      console.log('STATUS: ', status);
    }).catch((error) => {
      console.log('ERROR: ', error);
      const {
        onError,
      } = this.props;
      const message = 'Mail services are not available';
      onError(message);
    });
  }

  sendMail = async () => {

  }

  handlerCallContact = () => {
    const callScheme = 'tel:+84909289955';
    Linking.canOpenURL(callScheme).then((supported) => {
      if (supported) {
        Linking.openURL(callScheme);
      } else {
        console.log('Don\'t know how to open URI: ', callScheme);
      }
    });
  }

  // =============RENDER LAYOUT=======================
  renderEmailField = (user) => {
    const iOSField = (
      <View style={stylesLogin.marginVertical}>
        <View style={stylesLogin.iconContainer}>
          <FontAwesome name={iconName} size={Size.tabIconSize} color={APP_BACKGROUND} />
        </View>
        <TextInput
          style={stylesLogin.textInputField}
          value={user}
          underlineColorAndroid="rgba(0,0,0,0)"
          onChangeText={(username) => { this.setState({ username }); }}
          placeholder={I18n.t('username')}
        />
      </View>
    );

    // const androidField = (
    //   <View style={stylesLogin.marginVertical}>
    //     <Hoshi
    //       style={stylesLogin.inputStyle}
    //       inputStyle={stylesLogin.input}
    //       labelStyle={stylesLogin.inputLabel}
    //       label={I18n.t('username')}
    //       // this is used as active border color
    //       borderColor={APP_BACKGROUND}
    //       // this is used to set backgroundColor of label mask.
    //       // please pass the backgroundColor of your TextInput container.
    //       maskColor={TRANSPARENT}
    //       onChangeText={(username) => { this.setState({ username }); }}
    //     />
    //   </View>
    // );

    return iOSField;
  }

  renderPasswordField = (pass) => {
    const iosField = (
      <View style={stylesLogin.marginVertical}>
        <View style={stylesLogin.iconContainer}>
          <FontAwesome name={passName} size={Size.tabIconSize} color={APP_BACKGROUND} />
        </View>
        <TextInput
          style={stylesLogin.textInputField}
          value={pass}
          underlineColorAndroid="rgba(0,0,0,0)"
          onChangeText={(password) => { this.setState({ password }); }}
          placeholder={I18n.t('password')}
          secureTextEntry
        />
      </View>
    );

    // const androidField = (
    //   <View style={stylesLogin.marginVertical}>
    //     <Hoshi
    //       style={stylesLogin.inputStyle}
    //       inputStyle={stylesLogin.input}
    //       labelStyle={stylesLogin.inputLabel}
    //       label={I18n.t('password')}
    //       // this is used as active border color
    //       borderColor={APP_BACKGROUND}
    //       // this is used to set backgroundColor of label mask.
    //       // please pass the backgroundColor of your TextInput container.
    //       maskColor={TRANSPARENT}
    //       secureTextEntry
    //       onChangeText={(password) => { this.setState({ password }); }}
    //     />
    //   </View>
    // );
    return iosField;
  }

  renderActionLink = () => {
    const view = (
      <View style={stylesLogin.linkContainer}>
        <TouchableOpacity onPress={this.handlerRegisterClick}>
          <Text style={stylesLogin.linkStyle}>{I18n.t('register')}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handlerRegisterClick}>
          <Text style={stylesLogin.linkStyle}>{I18n.t('forgot_password')}</Text>
        </TouchableOpacity>
      </View>
    );

    return view;
  }

  renderContactLink = () => {
    const view = (
      <View style={stylesLogin.linkContactContainer}>
        <TouchableOpacity
          style={stylesLogin.contactLinkStyle}
          onPress={this.handlerSendMail}
        >
          <FontAwesome name={mail} size={Size.contactIconSize} color={APP_BACKGROUND} />
          <Text style={[stylesLogin.linkStyle, { marginLeft: '5%' }]}>{contactInfo}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={stylesLogin.contactLinkStyle}
          onPress={this.handlerCallContact}
        >
          <FontAwesome name={phone} size={Size.contactIconSize} color={APP_BACKGROUND} />
          <Text style={[stylesLogin.linkStyle, { marginLeft: '5%' }]}>{phoneNumber}</Text>
        </TouchableOpacity>
      </View>
    );

    return view;
  }

  render() {
    const {
      style,
    } = this.props;
    const { username, password } = this.state;

    return (
      <View style={[style]}>
        {
          this.renderEmailField(username)
        }
        {
          this.renderPasswordField(password)
        }
        <Button
          style={stylesLogin.loginButton}
          textStyle={{ color: APP_NAME, fontWeight: 'bold' }}
          onPress={this.handlerLoginClick}
        >
          {I18n.t('login')}
        </Button>
        {
          this.renderActionLink()
        }
        {
          this.renderContactLink()
        }
      </View>
    );
  }
}

LoginForm.propTypes = {
  onLoginRequest: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  style: PropTypes.any, // eslint-disable-line
};

export default LoginForm;
