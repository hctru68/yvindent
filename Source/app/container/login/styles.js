import { StyleSheet } from 'react-native';
import { APP_BACKGROUND_2, APP_NAME } from '../../common/constants/color';
import Size from '../../common/constants/size';

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: Size.width,
    height: Size.height,
    zIndex: -1000,
    resizeMode: 'cover',
  },
  layerHidden: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: Size.width,
    height: Size.height,
    zIndex: -1000,
    backgroundColor: APP_BACKGROUND_2,
  },
  container: {
    flex: 1,
  },
  containerHeader: {
    flex: 0.5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: Size.logoWidth,
    height: Size.logoHeight,
  },
  title: {
    marginTop: Size.spaceLogo,
    fontSize: Size.titleAppSize,
    color: APP_NAME,
    fontWeight: 'bold',
  },
  containerLogin: {
    flex: 0.45,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  loginForm: {
    flex: 0.55,
    paddingVertical: Size.verticalSpacingLogin,
  },
});

export default styles;
