import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import styles from './styles';
import userActions from '../../store/actions/userActions';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.actions = null;
    this.state = {
    };
  }

  componentDidMount() {
    this.mapVariableToProp();
    this.actions.checkUserExist();
  }

  mapVariableToProp = () => {
    const {
      actions,
    } = this.props;
    this.actions = actions;
  }

  render() {
    console.log('Splash Screen');

    return (
      <SafeAreaView style={styles.container} />
    );
  }
}

SplashScreen.propTypes = {
  actions: PropTypes.any, // eslint-disable-line
};

const mapStateToProps = (state) => {
  const { apps } = state;
  return {
    apps,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...userActions,
  }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
