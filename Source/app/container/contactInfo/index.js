import React, { Component } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {
  SafeAreaView,
} from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import BallIndicator from '../../components/ball-indicator';

import I18n from '../../common/utils/languageHelper';
import styles from './styles';
import userActions from '../../store/actions/userActions';

import LoginForm from './login-form';
import ImageConstant from '../../common/constants/image';
import commontStyles from '../../components/commonStyle';
import { WHITE_DARKEN_BACKGROUND } from '../../common/constants/color';

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRequesting: false,
    };
  }

  componentDidMount() {
    this.mapVariableToProp(this.props);
  }

  mapVariableToProp = () => {
    const {
      actions,
    } = this.props;
    this.actions = actions;
  }

  // ======================HANDLER EVENT ===================================
  onLoginRequest = (username, password) => {
    this.actions.requestLogin(username, password);
  }

  // ======================RENDER LAYOUT ===================================
  renderFormLogin = () => (
    <View style={styles.containerLogin}>
      <View style={styles.logo} />
      <LoginForm
        style={styles.loginForm}
        onLoginRequest={this.onLoginRequest}
      />
    </View>
  );

  render() {
    console.log('Login');
    const {
      isRequesting,
    } = this.state;

    return (
      <SafeAreaView
        forceInset={{ top: 'always' }}
        style={styles.container}
      >
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            scrollEnable={false}
            contentContainerStyle={styles.container}
          >
            {
              this.renderFormLogin()
            }
          </ScrollView>
        </KeyboardAvoidingView>
        <Image
          source={ImageConstant.BackgroundImage}
          style={styles.backgroundImage}
        />
        <View style={styles.layerHidden} />
        {
          isRequesting && (
            <View style={commontStyles.overlay}>
              <BallIndicator color={WHITE_DARKEN_BACKGROUND} />
            </View>
          )
        }
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    apps,
    users,
  } = state;
  return {
    apps,
    users,
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    ...userActions,
  }, dispatch),
});

Contact.propTypes = {
  apps: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  users: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  actions: PropTypes.any, // eslint-disable-line
};

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
