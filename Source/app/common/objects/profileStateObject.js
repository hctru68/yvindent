const initialData = () => ({
    listProfile: null,
    resultUpdateProfile: null,
    resultUploadBase64: null,
});

const wrapProfileData = data => ({
    listProfile: data,
});

const wrapUpdateProfile = data => ({
     resultUpdateProfile: data,
});

const wrapUploadBase64 = data => ({
    resultUploadBase64: data,

});
const profile = {
    initialData,
    wrapProfileData,
    wrapUpdateProfile,
    wrapUploadBase64
};

export default profile;
