const initialData = () => ({
  user: null,
  token: null,
});

const wrapUserLoginData = data => ({
  token: data.token,
});

const userStateObject = {
  initialData,
  wrapUserLoginData,
};

export default userStateObject;
