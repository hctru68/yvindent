const initialData = () => ({
    listInbox: null,
    listSend: [],
    stateDelete: null,
});

const wrapInboxLoginData = data => ({
    listInbox: data,
});
const wrapInboxDeleteById = data => ({
    stateDelete: data,
});
const wrapListSendData = data => ({
    listSend: data,
});

const inboxStateObject = {
    initialData,
    wrapInboxLoginData,
    wrapInboxDeleteById,
    wrapListSendData,
};

export default inboxStateObject;
