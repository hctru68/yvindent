const initialData = () => ({
    detailMail: null,
});

const wrapDetailMailData = data => ({
    detailMail: data,
});

const detailMailStateObject = {
    initialData,
    wrapDetailMailData,
};

export default detailMailStateObject;
