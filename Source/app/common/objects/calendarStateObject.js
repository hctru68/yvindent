import moment from 'moment';

const initialData = () => ({
  rawData: [],
  data: {},
});

const mapCalendarDataToComponent = (item) => {
  ////console.log('Item: ', item);
  const {
    start,
    end,
  } = item;
  const startTimeUnit = parseFloat(start.replace('/', '').replace('Date', '').replace('(', '').replace(')', ''));
  const endTimeUnit = parseFloat(end.replace('/', '').replace('Date', '').replace('(', '').replace(')', ''));
  // //console.log('Start Time Unit: ', startTimeUnit);
  // //console.log('End Time Unit: ', endTimeUnit);
  const dateArray = [];
  let currenDate = moment(startTimeUnit);
  const endDate = moment(endTimeUnit);
  // //console.log('Current Date: ', currenDate);

  while (currenDate < endDate) {
    const dateLabel = moment(currenDate).format('YYYY-MM-DD');
    dateArray.push(dateLabel);
    currenDate = moment(currenDate).add(1, 'days');
  }
  return {
    data: item,
    date: dateArray,
  };
};

const setCalendarData = (rawData) => {
  ////console.log('RawData: ', rawData);

  const data = {};
  rawData.forEach((e) => {
    const item = mapCalendarDataToComponent(e);
    ////console.log('Item: ', item);
    item.date.forEach((label) => {
      const itemCalendar = data[label];
      if (itemCalendar) {
        data[label].push(item.data);
      } else {
        data[label] = [item.data];
      }
    });
  });

  ////console.log('Data final: ', data);

  return {
    rawData,
    data,
  };
};

const calendarStateObject = {
  initialData,
  setCalendarData,
};

export default calendarStateObject;
