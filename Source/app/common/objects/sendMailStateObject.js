const initialData = () => ({
    sendMail: null,
});

const wrapSendMailData = data => ({
    sendMail: data,
});

const sendMailStateObject = {
    initialData,
    wrapSendMailData,
};

export default sendMailStateObject;
