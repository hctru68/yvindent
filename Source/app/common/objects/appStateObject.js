import {
  APP_ACTIONS,
} from '../../store/actionTypes';

const initialData = () => ({
  page: APP_ACTIONS.SPLASH_SCREEN,
  action: '',
  isRequesting: false,
  success: false,
  isFailure: false,
  errorMessage: '',
  isConnectedNetwork: null,
  msg: '',
  status: '',
});

const updateNewPage = newPage => ({
  page: newPage,
  isRequesting: false,
  success: false,
  isFailure: false,
  errorMessage: '',
});

const enableRequesting = type => ({
  action: type,
  isRequesting: true,
  success: false,
  isFailure: false,
  errorMessage: '',
});

const setRequestError = (message, action = '') => ({
  action,
  errorMessage: message,
  success: false,
  isFailure: true,
  isRequesting: false,
});

const setRequestSuccess = (action = '') => ({
  action,
  errorMessage: '',
  success: true,
  isFailure: false,
  isRequesting: false,
});

const setRequestUpdateProfile = (action = '', msg, status) => ({
    action,
    msg: msg,
    status: status,
});

const setRequestUploadBase64 = (action = '', status, filename) => ({
    action,
    status: status,
    filename: filename,
});

const appStateObject = {
  initialData,
  updateNewPage,
  enableRequesting,
  setRequestError,
  setRequestSuccess,

  setRequestUpdateProfile,
  setRequestUploadBase64,
};

export default appStateObject;
