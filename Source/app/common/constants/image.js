const BackgroundImage = require('../../../assets/images/background.png');
const Logo = require('../../../assets/logo.png');
const IconFeMale = require('../../../assets/avatar/icon-female.png');
const IconMale = require('../../../assets/avatar/icon-male.png');

const ImageConstant = {
    BackgroundImage,
    Logo,
    IconFeMale,
    IconMale,
};

export default ImageConstant;
