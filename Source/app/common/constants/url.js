export const API_DOMAIN = 'http://115.78.95.33';
export const API_URL = 'http://115.78.95.33/misd';
export const API_URL_AVATAR = 'http://115.78.95.33/UserFile/UserPhoto';
export const API_LOGIN = '/api/login_submit';
export const API_UPDATE_USER = '/api/profile';
export const API_GETALL_INBOX = '/api/inbox';
export const API_DELETE_INBOX = '/api/inboxdelete';
export const API_PUT_ISREAD_MAIL = '/api/setRead';

//profile
export const API_GETALL_PROFILE = '/api/profile';
export const API_UPDATE_PROFILE = 'api/updateprofile';
export const API_UPLOAD_BASE64_PROFILE = 'api/base64upload';
export const API_UPLOAD_FILE = '/api/base64uploadinbox';
export const  API_SEND_MAIL = '/api/inbox_submit';
export const API_GET_CALENDAR = 'api/calendar';
export const  API_GET_LIST_SEND = '/api/getlistsend';

export default {
    API_DOMAIN,
    API_URL,
    API_LOGIN,
    API_UPDATE_USER,
    API_GETALL_INBOX,
    API_GETALL_PROFILE,
    API_UPDATE_PROFILE,
    API_UPLOAD_BASE64_PROFILE,
    API_SEND_MAIL,
    API_GET_LIST_SEND,
    API_PUT_ISREAD_MAIL,
    API_UPLOAD_FILE,
};