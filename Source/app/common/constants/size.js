import {
    Dimensions,
    PixelRatio,
    Platform,
} from 'react-native';
// import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';

const {
    width,
    height
} = Dimensions.get('window');

const pixelRatio = PixelRatio.get();

const widthPercentageToDP = (widthPercent) => {
    const screenWidth = width;
    const elemWidth = parseFloat(widthPercent);

    return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100);
};

const heightPercentageToDP = (heightPercent) => {
    const screenHeight = height;
    const elemHeight = parseFloat(heightPercent);

    return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100);
};

const normalize = (size) => {
    if (Platform.OS === 'ios') {
        ////console.log('PIXEL RATIO: ', pixelRatio);
        let sizeScale = size;
        switch (pixelRatio) {
            case 3:
                if (Platform.isPad) {
                    sizeScale *= 1.1;
                } else {
                    sizeScale *= 1.25;
                }
                break;
            default:
                /**
                 * iPhone 4, 4 S
                 * iPhone 5, 5 c, 5 s
                 * iPhone 6
                 * Ipad Air
                 * Ipad Pro (9.7 inch)
                 */
                if (Platform.isPad) {
                    sizeScale *= 1.26;
                }
                break;
        }
        return Math.round(PixelRatio.roundToNearestPixel(sizeScale));
    }
    return Math.round(PixelRatio.roundToNearestPixel(size)) - 2;
};

const Size = {
    width,
    height,
    // Login Header Size
    logoWidth: widthPercentageToDP(42),
    logoHeight: widthPercentageToDP(42) * 862 / 916,
    verticalSpacingLogin: heightPercentageToDP(3),
    spaceLogo: heightPercentageToDP(5),
    titleAppSize: normalize(40),
    // Login Footer Size
    inputFieldHeight: heightPercentageToDP(6),
    inputTextSize: normalize(14),
    linkContainerHeight: heightPercentageToDP(3),
    linkFontSize: normalize(10),
    contactIconSize: normalize(15),

    // Bottom navigation bar
    tabHeight: heightPercentageToDP(7.5),
    tabIconSize: normalize(24),
    buttonIconSize: normalize(25),

    // title for each screen
    title: normalize(30),
    title_new_message: normalize(24),
    // MAIL
    titleMail: normalize(20),
    detailMail: normalize(14),
    attachFile: normalize(10),
    // Send Mail Input
    subjectInput: heightPercentageToDP(5),
    messageInput: heightPercentageToDP(30),
};

export default Size;
