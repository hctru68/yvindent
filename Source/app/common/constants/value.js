import { Platform } from 'react-native';
import { Constants } from 'expo';

export const DEVICE_ID = Platform.OS === 'ios' ? Constants.deviceId
  : Constants.deviceId;

export const DEVICE_OS = Platform.OS;
