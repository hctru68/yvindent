const GothamBold = require('../../../assets/fonts/Gotham-Bold.ttf');
const LatoBold = require('../../../assets/fonts/Lato-Bold.ttf');
const LatoBlack = require('../../../assets/fonts/Lato-Black.ttf');
const LatoLight = require('../../../assets/fonts/Lato-Light.ttf');
const LatoRegular = require('../../../assets/fonts/Lato-Regular.ttf');
const RobotoMonoMedium = require('../../../assets/fonts/RobotoMono-Medium.ttf');
const SFUIDisplaySemiBold = require('../../../assets/fonts/SF-UI-Display-Semibold.ttf');

const FontType = {
  GothamBold,
  LatoBold,
  LatoBlack,
  LatoLight,
  LatoRegular,
  RobotoMonoMedium,
  SFUIDisplaySemiBold,
};

export default FontType;
