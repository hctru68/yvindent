import React, { Component } from 'react';
import {
  Provider,
} from 'react-redux';
import {
  AppLoading,
  Asset,
  Font,
} from 'expo';
import {
  FontAwesome,
} from '@expo/vector-icons';
import store from './app/store';

import MainNavigator from './app/container/common-navigator';
import AppNavigatorService from './app/services/navigator/AppNavigatorService';
import I18n from './app/common/utils/languageHelper';
import ImageConstant from './app/common/constants/image';

class App extends Component {
  state = {
    isReady: false,
  }

  componentWillMount() {
    I18n.initAsync();
  }

  cacheImages = (images) => {
    return images.map((image) => {
      if (typeof image === 'string') {
        return Image.prefetch(image);
      }
      return Asset.fromModule(image).downloadAsync();
    });
  };

  cacheFonts = (fonts) => {
    return fonts.map(font => Font.loadAsync(font));
  }

  loadResourcesAsync = async () => (Promise.all([
    ...this.cacheImages([
      ImageConstant.BackgroundImage,
      ImageConstant.Logo,
    ]),
    ...this.cacheFonts([
      FontAwesome.font,
    ]),
  ]));

  // In this case, you might want to report the error to your error
  // reporting service, for example Sentry
  handleLoadingError = error => console.warn(error);

  handleFinishLoading = () => {
    this.setState({
      isReady: true,
    });
  };

  render() {
    const { isReady } = this.state;
    if (!isReady) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onFinish={this.handleFinishLoading}
          onError={this.handleLoadingError}
        />
      );
    }

    return (
      <Provider store={store}>
        <MainNavigator
          ref={(navigatorRef) => {
            AppNavigatorService.setContainer(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default App;
